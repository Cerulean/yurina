//
//  FDDepartureTime.h
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"
#import "FDDateFormatter.h"

@interface FDDepartureTime : FDBaseModel

@property (nonatomic, strong) NSDate *startTime;

@property (nonatomic, assign) NSInteger vacancy;

@property (nonatomic, strong) NSString *period;

@end
