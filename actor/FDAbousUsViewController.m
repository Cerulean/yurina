//
//  FDAbousUsViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/6.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDAbousUsViewController.h"

@implementation FDAbousUsViewController {
    
    UIScrollView *_scrollView;
    
    UIView *_contentView;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"关于我们";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _scrollView = [UIScrollView new];
    
    _contentView = [UIView new];
    _contentView.backgroundColor = ColorNormalBGWhite;
    
    UIImageView *icon = [[UIImageView alloc] initWithImage:FDImageWithName(@"Account_Icon")];
    
    UILabel *firstLabel = [UILabel labelWithText:@"超级活动家" Color:ColorAccountBlack FontSize:14 Alignment:NSTextAlignmentCenter Light:NO];
    
    UILabel *versionLabel = [UILabel labelWithText:@"版本 1.0" Color:ColorAccountBlack FontSize:10 Alignment:NSTextAlignmentCenter Light:NO];
    
    NSString *content = @"超级活动家是一个基于兴趣的活动体验平台和社区，\n提供来自全世界的实践体验机会，\n帮助青年群体提高自身综合素质、拓展国际视野，\n共同探索生活和梦想的更多可能。\n\n\n超级活动家成立于2014年，\n总部位于西安高新技术产业开发区，\n主要管理团队来自教育及旅游行业，\n希望以更加注重品质和体验的产品服务，\n满足年轻群体的兴趣实践需求。\n基于用户兴趣所在，\n超级活动家在研学旅行、\n社会实践、背景提升等板块提供服务。\n\n\n具体涵盖以下领域\n\n————————————————\n\n名校访学\n游访国际知名学府，拥抱世界\n\n————————\n\n实习实训\n企业机构实践训练，积累职业技巧与职场智慧\n\n————————\n\n公益旅行\n义工、支教、动物及环境保护项目，服务社会成长自我\n\n————————\n\n文化体验\n感知艺术、宗教、民俗文化韵味，感受人文之美\n\n————————\n\n户外探索\n探索运动与自然魅力，发现自我\n\n————————\n\n同城发现\n总有乐趣在身边，这里有一座城市的新奇与好玩\n\n————————\n\n超级活动家希望通过自身的努力，\n为青年群体提供更高品质的服务和体验；\n不仅作为兴趣实践服务的提供者，\n更是健康、积极生活方式倡导者；\n与用户一起探索将教育、文化、公益、户外与旅行\n相结合的生活美学。\n";
    
    UILabel *contentLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:12 Alignment:NSTextAlignmentCenter Light:YES];
    contentLabel.numberOfLines = 0;
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 12;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSMutableAttributedString *info = [[NSAttributedString attributedStringWithString:content Color:ColorCellText FontSize:12] mutableCopy];
    [info addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, content.length)];

    
    contentLabel.attributedText = info;
    
    [self.view addSubview:_scrollView];
    
    [_scrollView addSubview:_contentView];
    
    [_contentView addSubview:icon];
    [_contentView addSubview:firstLabel];
    [_contentView addSubview:versionLabel];
    [_contentView addSubview:contentLabel];
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@0);
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(@0);
        make.width.equalTo(@(SCREEN_WIDTH));
    }];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(@20);
    }];
    
    [firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(icon.mas_bottom).offset(10);
    }];
    
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(firstLabel.mas_bottom).offset(5);
    }];
    
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(versionLabel.mas_bottom).offset(25);
        make.bottom.equalTo(@(-20));
    }];
    
    [_contentView layoutIfNeeded];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self updateNaviBarType:FDNaviBarTypeWhite];
    
    _scrollView.contentSize = _contentView.frame.size;
}

@end
