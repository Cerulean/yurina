//
//  FDCredentialViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/5.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDCredentialViewController.h"
#import "FDAccountService.h"

@implementation FDCredentialViewController {
    
    UIScrollView *_scrollView;
    
    UIView *_contentView;
    
    UIImageView *_backgroundView;
    
    UILabel *_orderLabel;
    
    UILabel *_statusLabel;
    
    UIView *_firstLine;
    
    UILabel *_titleLabel;
    
    UIView *_secondLine;
    
    UILabel *_periodLabel;
    
    UILabel *_packageLabel;
    
    UILabel *_nameLabel;
    
    UILabel *_telLabel;
    
    UILabel *_emailLabel;
    
    UILabel *_addressLabel;
    
    UILabel *_channelLabel;
    
    UILabel *_dateLabel;
    
    UILabel *_feeLabel;
    
    UIView *_thirdLine;
    
    UIImageView *_avatar;
    
    UILabel *_tipLabel;
    
    UILabel *_helpLabel;
    
    UILabel *_countLabel;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"支付证书";
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadSubViews];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    WeakSelf;
    
    [self.viewModel fetchModelWithType:FDFetchModeNew Callback:^(NSArray *objects, FDBaseModel *model, NSError *error) {
        
        StrongSelf;
        
        if (model && [model isKindOfClass:[FDCredential class]]) {
            [s_self bindWithModel:(FDCredential *)model];
        }
        
    }];
}

- (void)loadSubViews {
    
    self.view.backgroundColor = ColorCredentialLightRed;
    
    _backgroundView = [[UIImageView alloc] initWithImage:FDImageWithName(@"Order_Credential_BG")];
    _backgroundView.userInteractionEnabled = YES;
    
    _scrollView = [UIScrollView new];
    
    _contentView = [UIView new];
    _contentView.backgroundColor = ColorNormalBGWhite;
    
    _orderLabel = [UILabel labelWithText:@"" Color:ColorCredentialBlack FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
    
    _statusLabel = [UILabel labelWithText:@"" Color:ColorCredentialBlack FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _firstLine = [UIView new];
    _firstLine.backgroundColor = ColorCredentialBlack;
    
    _titleLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:12 Alignment:NSTextAlignmentLeft Light:NO];
    _titleLabel.numberOfLines = 2;
    
    _secondLine = [UIView new];
    _secondLine.backgroundColor = ColorCellText;
    
    _periodLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _packageLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _nameLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _telLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _emailLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _addressLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _channelLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _dateLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _feeLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _countLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _thirdLine = [UIView new];
    _thirdLine.backgroundColor = ColorCellText;
    
    _avatar = [UIImageView new];
    [_avatar setClipsToBounds:YES];
    [_avatar setContentMode:UIViewContentModeScaleAspectFill];
    [_avatar.layer setCornerRadius:30];
    
    _tipLabel = [UILabel labelWithText:@"" Color:ColorCredentialBlack FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
    _tipLabel.numberOfLines = 2;
    _tipLabel.textAlignment = NSTextAlignmentCenter;
    
    _helpLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:NO];
    _helpLabel.numberOfLines = 2;
    _helpLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:_backgroundView];
    
    [_backgroundView addSubview:_scrollView];

    [_scrollView addSubview:_contentView];
    
    [_contentView addSubview:_orderLabel];
    [_contentView addSubview:_statusLabel];
    [_contentView addSubview:_firstLine];
    [_contentView addSubview:_titleLabel];
    [_contentView addSubview:_secondLine];
    [_contentView addSubview:_periodLabel];
    [_contentView addSubview:_packageLabel];
    [_contentView addSubview:_nameLabel];
    [_contentView addSubview:_telLabel];
    [_contentView addSubview:_emailLabel];
    [_contentView addSubview:_addressLabel];
    [_contentView addSubview:_channelLabel];
    [_contentView addSubview:_dateLabel];
    [_contentView addSubview:_feeLabel];
    [_contentView addSubview:_countLabel];
    [_contentView addSubview:_thirdLine];
    [_contentView addSubview:_avatar];
    [_contentView addSubview:_tipLabel];
    [_contentView addSubview:_helpLabel];
    
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.right.equalTo(@(-10));
        make.top.equalTo(@20);
        make.bottom.equalTo(@(-15));
    }];
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(_backgroundView);
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(@0);
        make.width.equalTo(@(SCREEN_WIDTH - 20));
    }];
    
    [_orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@20);
        make.right.equalTo(_statusLabel.mas_left).offset(-20);
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_orderLabel);
        make.right.equalTo(@(-20));
    }];
    
    [_firstLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.top.equalTo(_orderLabel.mas_bottom).offset(10);
        make.height.equalTo(@0.5);
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@30);
        make.top.equalTo(_firstLine.mas_bottom).offset(20);
    }];
    
    [_secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@30);
        make.right.equalTo(@(-30));
        make.top.equalTo(_titleLabel.mas_bottom).offset(10);
        make.height.equalTo(@0.5);
    }];
    
    [_packageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_secondLine.mas_bottom).offset(20);
    }];
    
    [_periodLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_packageLabel.mas_bottom).offset(20);
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_periodLabel.mas_bottom).offset(20);
    }];
    
    [_telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_nameLabel.mas_bottom).offset(20);
    }];
    
    [_emailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_telLabel.mas_bottom).offset(20);
    }];
    
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_emailLabel.mas_bottom).offset(20);
    }];
    
    [_channelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_addressLabel.mas_bottom).offset(20);
    }];
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.top.equalTo(_channelLabel.mas_bottom).offset(20);
    }];
    
    [_feeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_dateLabel);
        make.top.equalTo(_dateLabel.mas_bottom).offset(20);
    }];
    
    [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_dateLabel);
        make.top.equalTo(_feeLabel.mas_bottom).offset(20);
    }];
    
    [_thirdLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@30);
        make.right.equalTo(@(-30));
        make.top.equalTo(_countLabel.mas_bottom).offset(20);
        make.height.equalTo(@0.5);
    }];
    
    [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(_thirdLine.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(_avatar.mas_bottom).offset(10);
    }];
    
    [_helpLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(_tipLabel.mas_bottom).offset(20);
        make.bottom.equalTo(@(-15));
    }];
}

- (void)bindWithModel:(FDCredential *)credential {
    
    _orderLabel.text = [NSString stringWithFormat:@"订单号：%@", [credential.orderID uppercaseString]];
    
    NSMutableAttributedString *infoString = [[NSAttributedString attributedStringWithString:@"订单状态："
                                                                                      Color:ColorCredentialBlack
                                                                                   FontSize:10] mutableCopy];
    
    [infoString appendAttributedString:[NSAttributedString attributedStringWithString:[FDOrder stringForStatus:credential.orderStatus]
                                                                                Color:ColorCredentialRed
                                                                             FontSize:10]];
    
    _statusLabel.attributedText = infoString;
    
    _titleLabel.text = credential.title;
    
    _periodLabel.text = [NSString stringWithFormat:@"义卖期数：%@", credential.period];
    
    _packageLabel.text = [NSString stringWithFormat:@"套餐选择：%@", credential.packageName];
    
    _nameLabel.text = [NSString stringWithFormat:@"联系人：%@", credential.personName];
    
    _telLabel.text = [NSString stringWithFormat:@"联系电话：%@", credential.personTel];
    
    _emailLabel.text = [NSString stringWithFormat:@"电子邮箱：%@", credential.personEmail];
    
    _addressLabel.text = [NSString stringWithFormat:@"收货地址：%@", credential.personAddress];
    
    _channelLabel.text = [NSString stringWithFormat:@"支付方式：%@", credential.channel];
    
    _dateLabel.text = [NSString stringWithFormat:@"下单时间：%@", [FDDateFormatter stringFromDate:credential.orderCreateDate]];
    
    _feeLabel.text = [NSString stringWithFormat:@"支付金额：%@", [NSString stringWithFormat:@"费用 CNY %.2f", credential.applyFee/ 100]];
    
    _countLabel.text = [NSString stringWithFormat:@"商品件数：%ld个", credential.count];
    
//    if (credential.projectFee > 0) {
//        _feeLabel.text = [_feeLabel.text stringByAppendingString:[NSString stringWithFormat:@" 项目费 CNY %.2f", credential.projectFee / 100]];
//    }
    
    FDUser *user = [FDAccountService currentUser];
    
    NSString *tipString = [NSString stringWithFormat:@"%@，", user.truename ? user.truename : user.nickname ? user.nickname : @""];
    
    NSString *tip = @"";
    
    switch (credential.orderStatus) {
        case FDOrderStatusNeedReview:
            tip = @"已经报名，正在审核";
            break;
        case FDOrderStatusNeedPayProjectFee:
            tip = @"审核通过，等待缴纳项目费";
            break;
        case FDOrderStatusWaitProjectBegin:
            tip = @"还未参与此项活动，请注意项目开始时间";
            break;
        case FDOrderStatusProjectProcessing:
            tip = @"义卖正在进行";
            break;
        case FDOrderStatusProjectFinished:
            tip = @"您已经确认收货";
            break;
        default:
            break;
    }
    
    tipString = [tipString stringByAppendingString:tip];
    
    _tipLabel.text = tipString;
    
    infoString = [[NSAttributedString attributedStringWithString:@"关于商品物流的任何问题，请咨询客服\n" Color:ColorProjectPlaceText FontSize:10] mutableCopy];
    [infoString appendAttributedString:[NSAttributedString attributedStringWithString:@"186-2935-7335" Color:ColorProjectPolicy FontSize:10]];
    
    _helpLabel.attributedText = infoString;
    
    [_avatar sd_setImageWithURL:[NSURL URLWithString:user.avatarURL]];
    
    [_contentView layoutIfNeeded];
    
    _scrollView.contentSize = CGSizeMake(_contentView.frame.size.width, _contentView.frame.size.height);
}

@end
