//
//  FDProjectViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectDetailViewController.h"
#import "FDProjectViewController.h"
#import "FDImageViewerController.h"
#import "FDProjectConsultView.h"
#import "FDWebViewController.h"
#import "FDProjectApplyView.h"
#import "FDProjectDateView.h"

@implementation FDProjectViewController {
    
    UIScrollView *_scrollView;
    
    UIView *_contentView;
    
    UIImageView *_topImageView;
    
    FDButton *_favoButton;
    
    UILabel *_projectFeeLabel;
    
    UILabel *_applyFeeLabel;
    
    UIView *_firstLine;
    
    UIView *_secondLine;
    
    UILabel *_titleLabel;
    
    UILabel *_tagLabel;
    
    UILabel *_applyLocationLabel;
    
    UIImageView *_applyLocationIcon;
    
    UILabel *_descriptionLabel;
    
    FDButton *_moreButton;
    
    UIScrollView *_imagesView;
    
    UIView *_thirdLine;
    
    UILabel *_periodLabel;
    
    UILabel *_periodContentLabel;
    
    FDButton *_applyDateButton;
    
    UIView *_fourthLine;
    
    FDButton *_feeIntroButton;
    
    FDButton *_qaButton;
    
    FDButton *_consultButton;
    
    FDButton *_applyButton;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"项目详情";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadSubViews];
    
    WeakSelf;
    
    
    [self.viewModel fetchModelWithType:FDFetchModeNew Callback:^(NSArray *objects, FDBaseModel *model, NSError *error) {
        
        StrongSelf;
        
        if (![model isKindOfClass:[FDProject class]]) {
            return ;
        }
        
        [s_self bindWithModel:(FDProject *)model];
        
    }];
}

- (void)loadSubViews {
    
    self.view.backgroundColor = ColorTableSection;
    
    _scrollView = [[UIScrollView alloc] init];
    
    _contentView = [UIView new];
    
    WeakSelf;
    
//    _favoButton = [FDButton buttonWithType:UIButtonTypeCustom ActionBlock:^(FDButton *button) {
//        
//        StrongSelf;
//        
//        BOOL selected = button.selected;
//        
//        FDProject *project = (FDProject *)s_self.viewModel.model;
//        
//        if (!selected) {
//            [FDProjectService favoProject:project.objectID Callback:^(BOOL success) {
//                if (success) {
//                    [button setSelected:!selected];
//                }
//            }];
//        } else {
//            [FDProjectService unfavoProject:project.objectID Callback:^(BOOL success) {
//                if (success) {
//                    [button setSelected:!selected];
//                }
//            }];
//        }
//        
//    }];
    
//    [_favoButton setImageName:@"Trip_Favo" AutoHighlight:NO AutoDisabled:NO AutoSelected:YES];
    
    _topImageView = [UIImageView new];
    _topImageView.contentMode = UIViewContentModeScaleAspectFill;
    _topImageView.userInteractionEnabled = YES;
    _topImageView.clipsToBounds = YES;
    
    _projectFeeLabel = [UILabel labelWithText:@"" Color:ColorNormalBGWhite FontSize:18 Alignment:NSTextAlignmentCenter Light:NO];
    _projectFeeLabel.backgroundColor = ColorProjectFeeLabelBG;
    _projectFeeLabel.alpha = 0.8;
    
//    _applyFeeLabel   = [UILabel labelWithText:@"" Color:ColorNormalBGWhite FontSize:10 Alignment:NSTextAlignmentCenter Light:NO];
//    _applyFeeLabel.backgroundColor = ColorProjectApplyLabelBG;
//    _applyFeeLabel.alpha = 0.8;
    
    _firstLine = [UIView new];
    _firstLine.backgroundColor = ColorCellLine;
    
    _titleLabel = [UILabel labelWithText:@"" Color:ColorProjectTextLabelBG FontSize:18 Alignment:NSTextAlignmentCenter Light:NO];
    _titleLabel.numberOfLines = 0;
    
    _secondLine = [UIView new];
    _secondLine.backgroundColor = ColorCellLine;
    
    _tagLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    
    _applyLocationIcon  = [[UIImageView alloc] initWithImage:FDImageWithName(@"Project_Location")];
    
    _applyLocationLabel = [UILabel labelWithText:@"线下义卖地点：" Color:ColorCellText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
    
    _descriptionLabel   = [UILabel labelWithText:@"" Color:ColorCellText FontSize:14 Alignment:NSTextAlignmentLeft Light:YES];
    _descriptionLabel.numberOfLines = 0;
    
    _moreButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"阅读更多" FontSize:14 ActionBlock:^(FDButton *button) {
        
        StrongSelf;
        
        if (!s_self.viewModel.model) {
            return ;
        }
        
        FDProjectDetailViewController *detailController = [FDProjectDetailViewController new];
        detailController.project = (FDProject *)s_self.viewModel.model;
        
        [s_self.navigationController pushViewController:detailController animated:YES];
        
    }];
    
    [_moreButton setTitleColor:ColorProjectMoreButton forState:UIControlStateNormal];
    [_moreButton.layer setCornerRadius:2.0];
    [_moreButton.layer setBorderWidth:0.5];
    [_moreButton.layer setBorderColor:ColorProjectMoreButton.CGColor];
    
    _imagesView = [UIScrollView new];
    _imagesView.pagingEnabled = YES;
    _imagesView.showsHorizontalScrollIndicator = NO;
    _imagesView.delaysContentTouches = YES;
    
    _thirdLine = [UIView new];
    _thirdLine.backgroundColor = ColorCellLine;
    
    _periodLabel = [UILabel labelWithText:@"义卖周期" Color:ColorProjectTextLabelBG FontSize:14 Alignment:NSTextAlignmentLeft Light:YES];
    
    _periodContentLabel = [UILabel labelWithText:@"" Color:ColorProjectTextLabelBG FontSize:14 Alignment:NSTextAlignmentRight Light:YES];
    
    _applyDateButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"商品库存" FontSize:14 ActionBlock:nil];
    
    [_applyDateButton setTitleColor:ColorProjectMoreButton forState:UIControlStateNormal];
    
    _fourthLine = [UIView new];
    _fourthLine.backgroundColor = ColorCellLine;
    
//    _feeIntroButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"费用说明 *" FontSize:14 ActionBlock:nil];
//    [_feeIntroButton setTitleColor:ColorProjectMoreButton forState:UIControlStateNormal];
    
//    _qaButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"使用协议 *" FontSize:14 ActionBlock:nil];
//    [_qaButton setTitleColor:ColorProjectMoreButton forState:UIControlStateNormal];
    
    _consultButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"咨询客服" FontSize:14 ActionBlock:nil];
    
    [_consultButton setTitleColor:ColorProjectMoreButton forState:UIControlStateNormal];
    [_consultButton setBackgroundColor:ColorNormalBGWhite];
    
    _applyButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"立刻购买" FontSize:14 ActionBlock:nil];
    
    [_applyButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
    [_applyButton setBackgroundColor:ColorProjectMoreButton];
    
    [self.view addSubview:_scrollView];
    [self.view addSubview:_consultButton];
    [self.view addSubview:_applyButton];
    
    [_scrollView addSubview:_contentView];
    
    [_contentView addSubview:_topImageView];
    
//    [_topImageView addSubview:_favoButton];
    [_topImageView addSubview:_projectFeeLabel];
    [_topImageView addSubview:_applyFeeLabel];
    
    [_contentView addSubview:_firstLine];
    [_contentView addSubview:_titleLabel];
    [_contentView addSubview:_secondLine];
    
    [_contentView addSubview:_tagLabel];
    
    [_contentView addSubview:_applyLocationIcon];
    [_contentView addSubview:_applyLocationLabel];
    
    [_contentView addSubview:_descriptionLabel];
    
    [_contentView addSubview:_moreButton];
    
    [_contentView addSubview:_imagesView];
    
    [_contentView addSubview:_thirdLine];
    
    [_contentView addSubview:_periodLabel];
    
    [_contentView addSubview:_periodContentLabel];
    
    [_contentView addSubview:_applyDateButton];
    
    [_contentView addSubview:_fourthLine];
    
    [_contentView addSubview:_feeIntroButton];
    
    [_contentView addSubview:_qaButton];
}

- (void)bindWithModel:(FDProject *)project {
    
    self.title = _topic ? _topic.title : project.title;
    
    [_topImageView sd_setImageWithURL:[NSURL URLWithString:[project.imageURLs firstObject]]];
    
    FDPackage *packge = [[project packages] firstObject];
    
    NSString *projectFee = [@(packge.fee / 100) stringValue];
    
    NSMutableAttributedString *projectFeeString = [[NSAttributedString attributedStringWithString:@"￥" Color:ColorNormalBGWhite FontSize:10] mutableCopy];
    [projectFeeString appendAttributedString:[NSAttributedString attributedStringWithString:projectFee Color:ColorNormalBGWhite FontSize:18]];
    [projectFeeString appendAttributedString:[NSAttributedString attributedStringWithString:@" CNY" Color:ColorNormalBGWhite FontSize:10]];
    
    _projectFeeLabel.attributedText = projectFeeString;
    
//    _applyFeeLabel.text = [NSString stringWithFormat:@"报名费：CNY %.2f", project.applyFee / 100];
    
    _titleLabel.text = project.title;
    
    for (NSString *tag in project.tags) {
        
        if (tag == [project.tags firstObject]) {
            _tagLabel.text = tag;
        } else {
            _tagLabel.text = [_tagLabel.text stringByAppendingString:[NSString stringWithFormat:@" / %@", tag]];
        }
    }
    
    _applyLocationLabel.text = [_applyLocationLabel.text stringByAppendingString:project.applyPlace];
    
    _descriptionLabel.text = project.intro;
    
    _periodContentLabel.text = [NSString stringWithFormat:@"%ld天", packge.duration];
    
    WeakSelf;
    
    _applyDateButton.actionBlock = ^(FDButton *button) {
        
        StrongSelf;
        
        FDProjectDateView *dateView = [[FDProjectDateView alloc] initWithShadowColor:[UIColor clearColor] Info:project.departureTimes];
        
        [dateView showWithView:s_self.navigationController.view];
    };
    
    _feeIntroButton.actionBlock = ^(FDButton *button) {
        
        StrongSelf;
        
        FDWebViewController *webController = [FDWebViewController new];
        webController.htmlString = project.feeIntro;
        
        [s_self.navigationController pushViewController:webController animated:YES];
        
    };
    
    _qaButton.actionBlock = ^(FDButton *button) {
        
        StrongSelf;
        
        FDWebViewController *webController = [FDWebViewController new];
        webController.linkURL = kURLDuty;
        
        [s_self.navigationController pushViewController:webController animated:YES];
        
    };
    
    _consultButton.actionBlock = ^(FDButton *button) {
        
        StrongSelf;
        
        FDProjectConsultView *consultView = [[FDProjectConsultView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9] Info:nil];
        
        [consultView showWithView:s_self.navigationController.view];
    };
    
    _applyButton.actionBlock = ^(FDButton *button) {
        
        StrongSelf;
        
        FDProjectApplyView *applyView = [[FDProjectApplyView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9] Info:project];
        
        [applyView showWithView:s_self.navigationController.view];
    };
    
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(@0);
        make.bottom.equalTo(_consultButton.mas_top);
    }];
    
    [_consultButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(@0);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH / 2, 50));
    }];
    
    [_applyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.equalTo(@0);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH / 2, 50));
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@0);
        make.width.equalTo(@(SCREEN_WIDTH));
    }];
    
    [_topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(@0);
        make.height.equalTo(@260);
    }];
    
    [_favoButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@20);
        make.right.equalTo(@(-20));
    }];
    
//    [_applyFeeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(@0);
//        make.bottom.equalTo(@(-40));
//        make.size.mas_equalTo(CGSizeMake(130, 20));
//    }];
    
    [_projectFeeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@0);
        make.bottom.equalTo(@(-40));
        make.size.mas_equalTo(CGSizeMake(130, 35));
    }];
    
    [_firstLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topImageView.mas_bottom).offset(20);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@0.5);
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_firstLine.mas_bottom).offset(10);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
    }];
    
    [_secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLabel.mas_bottom).offset(10);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@0.5);
    }];
    
    [_tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.top.equalTo(_secondLine.mas_bottom).offset(10);
    }];
    
    [_applyLocationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.top.equalTo(_tagLabel.mas_bottom).offset(20);
    }];
    
    [_applyLocationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_applyLocationIcon.mas_right).offset(5);
        make.centerY.equalTo(_applyLocationIcon);
    }];
    
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.top.equalTo(_applyLocationLabel.mas_bottom).offset(20);
    }];
    
    [_moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_descriptionLabel);
        make.top.equalTo(_descriptionLabel.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(65, 25));
    }];
    
    [_imagesView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@0);
        make.top.equalTo(_moreButton.mas_bottom).offset(20);
        make.height.equalTo(@260);
    }];
    
    _imagesView.contentSize = CGSizeMake(SCREEN_WIDTH * project.imageURLs.count, 260);
    
    for (NSInteger index = 0; index < project.imageURLs.count; ++index) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(index * SCREEN_WIDTH, 0, SCREEN_WIDTH, 260)];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView sd_setImageWithURL:[NSURL URLWithString:project.imageURLs[index]]];
        [imageView setBackgroundColor:ColorNormalPlaceholder];
        [imageView setClipsToBounds:YES];
        [imageView setUserInteractionEnabled:YES];
        [imageView setTapAction:^(UIView *view) {
            
            UIImage *image = [(UIImageView *)view image];
            
            [FDImageViewerController previewImage:image];
        }];
        
        [_imagesView addSubview:imageView];
    }
    
    [_thirdLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_imagesView.mas_bottom).offset(20);
        make.left.right.equalTo(@0);
        make.height.equalTo(@0.5);
    }];
    
    [_periodLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_thirdLine.mas_bottom).offset(20);
        make.left.equalTo(@20);
    }];
    
    [_periodContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_periodLabel);
        make.right.equalTo(@(-20));
    }];
    
    [_applyDateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_periodLabel);
        make.top.equalTo(_periodLabel.mas_bottom).offset(20);
    }];
    
    [_fourthLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_applyDateButton.mas_bottom).offset(20);
        make.left.equalTo(_applyDateButton);
        make.bottom.equalTo(@(-40));
        make.size.mas_equalTo(CGSizeMake(30, 0.5));
    }];
    
//    [_feeIntroButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_applyDateButton);
//        make.top.equalTo(_fourthLine.mas_bottom).offset(20);
//    }];
//    
//    [_qaButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_applyDateButton);
//        make.top.equalTo(_feeIntroButton.mas_bottom).offset(20);
//        make.bottom.equalTo(@(-40));
//    }];
    
    [_contentView layoutIfNeeded];
    
    _scrollView.contentSize = _contentView.frame.size;
}

@end
