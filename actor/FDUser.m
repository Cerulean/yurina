//
//  FDUser.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDUser.h"

@implementation FDUser

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"username"  : @"username",
             @"nickname"  : @"nickname",
             @"avatarURL" : @"avatar_url",
             @"hobbies"   : @"hobbies",
             };
}

@end
