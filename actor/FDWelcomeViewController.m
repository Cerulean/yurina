//
//  FDWelcomeViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/16.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDWelcomeViewController.h"
#import "FDAccountViewController.h"
#import "FDBindViewController.h"
#import "FDWebViewController.h"
#import "FDAccountService.h"

@interface FDWelcomeViewController ()

@end

@implementation FDWelcomeViewController {
    UIImageView *_backgroundView;
    UIImageView *_sloganView;
    
    FDButton *_wechatLoginButton;
    FDButton *_weiboLoginButton;
    
    FDButton *_registerButton;
    FDButton *_loginButton;
    
    UILabel *_agreementsLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _backgroundView = [[UIImageView alloc] initWithImage:FDImageWithName(@"Account_BG")];
    _backgroundView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [self.view addSubview:_backgroundView];
    
    [self loadNotificationCallback];
    [self loadSlgoganView];
    [self loadButtons];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNaviBarType:FDNaviBarTypeClear];
}

- (void)loadNotificationCallback {
    
    WeakSelf;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:KSSOLoginSuccessNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      
                                                      StrongSelf;
                                                      
                                                      [s_self dismissViewControllerAnimated:YES completion:nil];
        
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:KSSOLoginNeedBindNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      
                                                      StrongSelf;
                                                      
                                                      NSString *ssoID = note.userInfo[kSSOIDKey];
                                                      
                                                      FDLoginPlatformType type = [note.userInfo[kSSOTypeKey] integerValue];
                                                      
                                                      FDBindViewController *bindController = [FDBindViewController new];
                                                      bindController.ssoID = ssoID;
                                                      bindController.ssoType = type;
                                                      
                                                      [s_self.navigationController pushViewController:bindController animated:YES];
                                                      
                                                  }];
    
}

- (void)loadSlgoganView {
    _sloganView = [[UIImageView alloc] initWithImage:FDImageWithName(@"Account_Slogan")];
    _sloganView.hidden = YES;
    
    [self.view addSubview:_sloganView];
}

- (void)loadButtons {
    _wechatLoginButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"微信" FontSize:18 ActionBlock:^(FDButton *button) {
        
        [FDAccountService purposeLoginWithPlatform:FDLoginPlatformTypeWechat];
        
    }];
    
    [_wechatLoginButton setTitleColor:ColorWelcomeTextMain forState:UIControlStateNormal];
    [_wechatLoginButton setBackgroundImageName:@"Account_Wechat_BG" AutoHighlight:YES AutoDisabled:NO AutoSelected:NO];
    [_wechatLoginButton setImage:FDImageWithName(@"Account_Wechat") forState:UIControlStateNormal];
    [_wechatLoginButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, (SCREEN_WIDTH / 2) + 30)];
    [_wechatLoginButton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
    [_wechatLoginButton setHidden:![FDAccountService canLoginWithWechat]];
    
    [self.view addSubview:_wechatLoginButton];
    
    _weiboLoginButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"微博" FontSize:18 ActionBlock:^(FDButton *button) {
        
        [FDAccountService purposeLoginWithPlatform:FDLoginPlatformTypeWeibo];
        
    }];
    
    [_weiboLoginButton setTitleColor:ColorWelcomeTextMain forState:UIControlStateNormal];
    [_weiboLoginButton setBackgroundImageName:@"Account_Weibo_BG" AutoHighlight:YES AutoDisabled:NO AutoSelected:NO];
    [_weiboLoginButton setImage:FDImageWithName(@"Account_Weibo") forState:UIControlStateNormal];
    [_weiboLoginButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, (SCREEN_WIDTH / 2) + 30)];
    [_weiboLoginButton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
    
    [self.view addSubview:_weiboLoginButton];
    
    WeakSelf;
    
    _registerButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"注册" FontSize:18 ActionBlock:^(FDButton *button) {
        StrongSelf;
        
        FDAccountViewController *accountViewController = [FDAccountViewController new];
        accountViewController.logicType = FDAccountLogicTypeRegister;
        
        [s_self.navigationController pushViewController:accountViewController animated:YES];
    }];
    
    [_registerButton setTitleColor:ColorWelcomeTextMain forState:UIControlStateNormal];
    [_registerButton setImageEdgeInsets:UIEdgeInsetsMake(0, SCREEN_WIDTH - 40, 0, 0)];
    [_registerButton.layer setBorderWidth:0.5];
    [_registerButton.layer setBorderColor:ColorWelcomeTextMain.CGColor];
    [_registerButton.layer setCornerRadius:4.0];
    
    [self.view addSubview:_registerButton];
    
    _loginButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"登录" FontSize:18 ActionBlock:^(FDButton *button) {
        StrongSelf;
        
        FDAccountViewController *accountViewController = [FDAccountViewController new];
        accountViewController.logicType = FDAccountLogicTypeLogin;
        
        [s_self.navigationController pushViewController:accountViewController animated:YES];
    }];
    
    [_loginButton setTitleColor:ColorWelcomeTextMain forState:UIControlStateNormal];
    [_loginButton setImageEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
    [_loginButton.layer setBorderWidth:0.5];
    [_loginButton.layer setBorderColor:ColorWelcomeTextMain.CGColor];
    [_loginButton.layer setCornerRadius:4.0];
    
    [self.view addSubview:_loginButton];
    
    _agreementsLabel = [UILabel labelWithText:@"" Color:ColorWelcomeTextMain FontSize:12 Alignment:NSTextAlignmentCenter Light:YES];
    
    NSAttributedString *notice = [NSAttributedString attributedStringWithString:@"登录即表示我同意企鹅 · 义卖的" Color:ColorWelcomeTextMain];
    NSAttributedString *agreements = [NSAttributedString attributedStringWithString:@"《服务条款》" Color:ColorWelcomeWeibo];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:notice];
    
    [text appendAttributedString:agreements];
    
    _agreementsLabel.attributedText = text;
    _agreementsLabel.userInteractionEnabled = YES;
    _agreementsLabel.tapAction = ^(UIView *view) {
        
        FDWebViewController *webController = [FDWebViewController new];
        
        webController.linkURL = kURLDuty;
        
        [UIWindow presentViewController:webController Animated:YES Completion:nil];
    };
    
    [self.view addSubview:_agreementsLabel];
    
    [_sloganView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(SCREEN_HEIGHT / 5));
        make.centerX.equalTo(@0);
    }];
    
    [_wechatLoginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_sloganView.mas_bottom).offset(SCREEN_HEIGHT / 5);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@45);
    }];
    
    [_weiboLoginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_wechatLoginButton.mas_bottom).offset(10);
        make.left.right.height.equalTo(_wechatLoginButton);
    }];
    
    [_registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_weiboLoginButton.mas_bottom).offset(10);
        make.left.equalTo(_weiboLoginButton);
        make.right.equalTo(_weiboLoginButton.mas_centerX).offset(-5);
        make.height.equalTo(_wechatLoginButton);
    }];
    
    [_loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_registerButton);
        make.right.equalTo(_weiboLoginButton.mas_right);
        make.left.equalTo(_weiboLoginButton.mas_centerX).offset(5);
        make.height.equalTo(_wechatLoginButton);
    }];
    
    [_agreementsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.bottom.equalTo(@(-20));
    }];
}

@end
