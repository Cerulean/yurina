//
//  FDOrderCell.m
//  actor
//
//  Created by 王澍宇 on 16/3/19.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDOrderCell.h"

#import "FDCheckoutView.h"

@implementation FDOrderCell {
    UIView *_containerView;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.line.hidden = YES;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = ColorTableSection;
        
        _containerView = [UIView new];
        _containerView.backgroundColor = ColorNormalBGWhite;
        _containerView.layer.cornerRadius = 5.0;
        
        _numberLabel = [UILabel labelWithText:@"" Color:ColorOrderText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
        
        _titleLabel  = [UILabel labelWithText:@"" Color:ColorOrderText FontSize:14 Alignment:NSTextAlignmentLeft Light:NO];
        _titleLabel.numberOfLines = 2;
        
        _startDateLabel = [UILabel labelWithText:@"" Color:ColorOrderText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
        
        _icon = [UIImageView new];
        _icon.contentMode = UIViewContentModeScaleAspectFill;
        _icon.backgroundColor = ColorNormalPlaceholder;
        _icon.clipsToBounds = YES;
        
        _horLine = [UIView new];
        _horLine.backgroundColor = ColorCellLine;
        
        _statusLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
        
        _button = [FDButton buttonWithType:UIButtonTypeSystem ActionBlock:nil];
        [_button setTintColor:ColorOrderStatusText];
        [_button.titleLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightLight]];
        [_button.layer setCornerRadius:4.0];
        [_button.layer setBorderColor:ColorOrderStatusText.CGColor];
        [_button.layer setBorderWidth:0.5];
        
        _feeLabel = [UILabel labelWithText:@"" Color:ColorOrderText FontSize:12 Alignment:NSTextAlignmentRight Light:YES];
        
//        _statusImageView = [UIImageView new];
        
        ContentViewAddSubView(_containerView);
        
        [_containerView addSubview:_numberLabel];
        [_containerView addSubview:_titleLabel];
        [_containerView addSubview:_startDateLabel];
        [_containerView addSubview:_icon];
        [_containerView addSubview:_horLine];
        [_containerView addSubview:_statusLabel];
        [_containerView addSubview:_button];
        [_containerView addSubview:_feeLabel];
//        [_containerView addSubview:_statusImageView];
        
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(10));
            make.top.equalTo(@(5));
            make.right.equalTo(@(-10));
            make.bottom.equalTo(@(-5));
        }];
        
//        [_statusImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.bottom.equalTo(@0);
//            make.width.equalTo(@10);
//        }];
        
        [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@10);
            make.top.equalTo(@10);
        }];
        
        [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_numberLabel);
            make.top.equalTo(_numberLabel.mas_bottom).offset(10);
            make.size.mas_equalTo(CGSizeMake(110, 70));
        }];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_icon);
            make.left.equalTo(_icon.mas_right).offset(10);
            make.right.equalTo(@(-5));
        }];
        
        [_startDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleLabel.mas_bottom).offset(15);
            make.left.equalTo(_titleLabel);
        }];
        
        [_horLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_icon.mas_bottom).offset(10);
            make.left.equalTo(_icon);
            make.right.equalTo(@(-5));
            make.height.equalTo(@0.5);
        }];
        
        [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_icon);
            make.top.equalTo(_horLine.mas_bottom).offset(15);
        }];
        
        [_button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_statusLabel);
            make.right.equalTo(@(-10));
            make.size.mas_equalTo(CGSizeMake(85, 20));
        }];
        
        [_feeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-10));
            make.top.equalTo(_statusLabel);
        }];
    }
    return self;
}

- (void)bindWithModel:(FDBaseModel *)model {
    
    self.model = model;
    
    if (![self.model isKindOfClass:[FDOrder class]]) {
        return;
    }
    
    FDOrder *order = (FDOrder *)self.model;
    
    _numberLabel.text = [NSString stringWithFormat:@"订单号：%@", [order.objectID uppercaseString]];
    
    _titleLabel.text  = order.title;
    
    NSDateComponents *components = [FDDateFormatter dateComponentsFromDate:order.createDate];
    
    _startDateLabel.text = [NSString stringWithFormat:@"出行日期：%ld.%ld.%ld", components.year, components.month, components.day];
    
    [_icon sd_setImageWithURL:[NSURL URLWithString:order.imageURL]];
    
    NSString *statusPrefix = @"";
    NSString *statusSuffix = @"";
    
    UIImage *statusImage = nil;
    
    BOOL buttonHidden = YES;
    
    NSString *buttonTitle = @"";
    
    components = [FDDateFormatter dateComponentsFromDate:order.departureTime];

    switch (order.status) {
        case FDOrderStatusNeedPayApplyFee:
            break;
        case FDOrderStatusNeedReview:
            statusPrefix = @"订单状态：";
            statusSuffix = @"审核中";
            statusImage = FDImageWithName(@"Trip_Yellow");
            buttonTitle = @"申请退款";
            buttonHidden = NO;
            break;
        case FDOrderStatusNeedPayProjectFee:
            statusPrefix = @"订单状态：";
            statusSuffix = @"审核通过";
            statusImage = FDImageWithName(@"Trip_Red");
            buttonTitle = @"缴纳项目费";
            buttonHidden = NO;
            break;
        case FDOrderStatusWaitProjectBegin:
            statusPrefix = @"距离开始剩余：";
            statusSuffix = [self generateTimeStatusWithDate:order.departureTime];
            statusImage = FDImageWithName(@"Trip_Yellow");
            buttonTitle = @"申请退款";
            buttonHidden = NO;
            break;
        case FDOrderStatusProjectProcessing:
            statusPrefix = @"订单状态：";
            statusSuffix = @"正在进行";
            statusImage = FDImageWithName(@"Trip_Red");
            break;
        case FDOrderStatusProjectFinished:
            statusPrefix = @"结束时间：";
            statusSuffix = [NSString stringWithFormat:@"%ld.%ld.%ld", components.year, components.month, components.day];
            statusImage = FDImageWithName(@"Trip_Black");
            break;
        case FDOrderStatusProjectRefunding:
            statusPrefix = @"订单状态：";
            statusSuffix = @"正在申请退款";
            statusImage = FDImageWithName(@"Trip_Black");
            break;
        case FDOrderStatusProjectRefunded:
            statusPrefix = @"订单状态：";
            statusSuffix = @"已经退款";
            statusImage = FDImageWithName(@"Trip_Black");
            break;
        case FDOrderStatusProjectRejected:
            statusPrefix = @"订单状态：";
            statusSuffix = @"审核未通过";
            statusImage = FDImageWithName(@"Trip_Black");
            break;
        default:
            break;
    }
    
    NSMutableAttributedString *statusString = [[NSAttributedString attributedStringWithString:statusPrefix Color:ColorCellText FontSize:12] mutableCopy];
    [statusString appendAttributedString:[NSAttributedString attributedStringWithString:statusSuffix Color:ColorOrderStatusText FontSize:12]];
    
    [_statusLabel setAttributedText:statusString];
    [_statusImageView setImage:statusImage];
    
    [_feeLabel setText:[NSString stringWithFormat:@"%ld元 × %ld个", (NSInteger)order.applyFee / 100, order.count]];
    
    WeakSelf;
    
    [_button setTitle:buttonTitle forState:UIControlStateNormal];
    [_button setHidden:buttonHidden];
    [_button setActionBlock:^(FDButton *button) {
        
        if (order.status >= FDOrderStatusNeedReview && order.status <= FDOrderStatusWaitProjectBegin) {
            
            StrongSelf;
            
            if (order.status == FDOrderStatusNeedPayProjectFee) {
                
                [FDOrderService getCheckoutWithOrderID:order.objectID Callback:^(BOOL success, FDCheckout *checkout){
                    
                    if (success && checkout) {
                        
                        FDCheckoutView *checkoutView = [[FDCheckoutView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9]
                                                                                              Info:checkout
                                                                                            Type:FDOrderCheckoutTypeProjectFee];
                        
                        [checkoutView showWithView:[[(UIViewController *)s_self.tableView.delegate navigationController] view]];
                    }
                    
                }];
                
            } else {
                
                [FDAlert alertWithTitle:@"申请退款" Message:@"您确认要申请退款吗？" Stressed:YES confirmAction:^{
                    
                    [FDOrderService refundWithOrderID:order.objectID Callback:^(BOOL success) {
                        
                        [s_self.tableView.mj_header beginRefreshing];
                        
                    }];
                    
                }];
            }
            
        }
        
    }];
}

- (NSString *)generateTimeStatusWithDate:(NSDate *)date {
    
    NSString *status = nil;
    
    NSTimeInterval offsetTime = [[NSDate date] timeIntervalSinceDate:date];
    
    if (offsetTime < 3600 * 24) {
        
        status = [NSString stringWithFormat:@"%ld小时", (NSInteger)offsetTime / 3600];
        
    } else {
        
        NSInteger day  = offsetTime / 3600 / 24;
        NSInteger hour = offsetTime / 3600 - day * 24;
        
        status = [NSString stringWithFormat:@"%ld天%ld小时", day, hour];
    }
    
    return status;

}

@end
