//
//  FDModifyViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDModifyViewController.h"
#import "FDAccountService.h"

@interface FDModifyViewController ()

@end

@implementation FDModifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    FDBarButtonItem *item = [FDBarButtonItem itemWithTitle:@"确定" Color:ColorNormalNaviItem Target:self Action:@selector(confirm:)];
    
    self.navigationItem.rightBarButtonItem = item;
    
    UITableViewCell *cell = [UITableViewCell new];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH - 40, 45)];
    _textField.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    _textField.textColor = ColorCellText;
    _textField.placeholder = _prompt;
    
    [cell.contentView addSubview:_textField];
    
    self.cellsArray = [@[@[cell]] mutableCopy];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNaviBarType:FDNaviBarTypeWhite];
}

- (void)confirm:(FDBarButtonItem *)sender {
    
    if (!_textField.text.length) {
        return [FDAlert alertWithTitle:@"错误" Message:@"修改内容不能为空"];
    }
    
    if ([_key isEqualToString:@"nickname"]) {
        if (_textField.text.length > 20) {
            return [FDAlert alertWithTitle:@"错误" Message:@"字符限制在20字"];
        }
    }
    
    if ([_key isEqualToString:@"truename"]) {
        if (_textField.text.length > 10) {
            return [FDAlert alertWithTitle:@"错误" Message:@"字符限制在10字"];
        }
    }
    
    if ([_key isEqualToString:@"email"]) {
        if (![FDValidater validateEmail:_textField.text]) {
            return [FDAlert alertWithTitle:@"错误" Message:@"邮箱格式不合法"];
        }
    }
    
    NSArray *hobbies = nil;
    
    if ([_key isEqualToString:@"hobbies"]) {
        
        hobbies = [_textField.text componentsSeparatedByString:@" "];
        
        if (!hobbies.count) {
            return [FDAlert alertWithTitle:@"错误" Message:@"爱好不能为空"];
        }
    }
    
    id value = [_key isEqualToString:@"hobbies"] ? hobbies : _textField.text;
    
    [FDAccountService modifyInfoWithParms:@{_key : value} Callback:^(BOOL success) {
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

@end
