//
//  FDTag.h
//  actor
//
//  Created by 王澍宇 on 16/3/22.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDTag : FDBaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *imageURL;

@end
