//
//  FDTripCell.m
//  actor
//
//  Created by 王澍宇 on 16/3/19.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectCell.h"
#import "FDProjectService.h"
#import "FDProjectViewController.h"

@implementation FDProjectCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.line.hidden = YES;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        WeakSelf;
        
        self.contentView.tapAction = ^(UIView *view) {
            
            StrongSelf;
            
            if (![s_self.model isKindOfClass:[FDProject class]]) {
                return ;
            }
            
            FDProject *project = (FDProject *)s_self.model;
            
            FDProjectViewController *projectViewController = [FDProjectViewController new];
            projectViewController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"project/info"];
            projectViewController.viewModel.prams[@"project_id"] = project.objectID;
            
            FDBaseViewController *viewController = (FDBaseViewController *)s_self.tableView.delegate;
            
            [viewController.navigationController pushViewController:projectViewController animated:YES];
            
        };
        
//        _favoButton = [FDButton buttonWithType:UIButtonTypeCustom ActionBlock:^(FDButton *button) {
//            
//            StrongSelf;
//            
//            BOOL selected = button.selected;
//            
//            FDProject *project = (FDProject *)s_self.model;
//            
//            if (!selected) {
//                [FDProjectService favoProject:project.objectID Callback:^(BOOL success) {
//                    if (success) {
//                        [button setSelected:!selected];
//                    } else {
//                        [button setSelected:YES];
//                    }
//                }];
//            } else {
//                [FDProjectService unfavoProject:project.objectID Callback:^(BOOL success) {
//                    if (success) {
//                        [button setSelected:!selected];
//                    }
//                }];
//            }
//            
//        }];
//        
//        [_favoButton setImageName:@"Trip_Favo" AutoHighlight:NO AutoDisabled:NO AutoSelected:YES];
        
        _titleLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:14 Alignment:NSTextAlignmentLeft Light:NO];
        _titleLabel.numberOfLines = 1;
        
        _introLabel = [UILabel labelWithText:@"" Color:ColorNormalBGWhite FontSize:12 Alignment:NSTextAlignmentLeft Light:NO];
        _introLabel.numberOfLines = 2;
        
        _infoLabel  = [UILabel labelWithText:@"" Color:ColorNormalBGWhite FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _backgroundImageView = [UIImageView new];
        _backgroundImageView.backgroundColor = ColorNormalPlaceholder;
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImageView.clipsToBounds = YES;
        
        ContentViewAddSubView(_backgroundImageView);
        ContentViewAddSubView(_titleLabel);
        ContentViewAddSubView(_introLabel);
        ContentViewAddSubView(_infoLabel);
//        ContentViewAddSubView(_favoButton);
        
//        [_favoButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(@15);
//            make.right.equalTo(@(-15));
//        }];
        
        [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.bottom.equalTo(_backgroundImageView).offset(-10);
        }];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_backgroundImageView.mas_bottom).offset(10);
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
        }];
        
        [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_titleLabel);
            make.top.equalTo(_titleLabel.mas_bottom).offset(5);
        }];
        
        [_backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(@0);
            make.height.equalTo(@240);
        }];
        
    }
    return self;
}

- (void)bindWithModel:(FDBaseModel *)model {
    if (![model isKindOfClass:[FDProject class]]) {
        return;
    }
    
    self.model = model;
    
    FDProject *project = (FDProject *)self.model;
    
    _titleLabel.text = project.title;
    
//    _introLabel.text = project.intro;
    
    NSMutableAttributedString *info = [NSMutableAttributedString new];
    
    FDPackage *package = [[project packages] firstObject];
    
    [info appendAttributedString:[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@"CNY %.2f · ", package.fee / 100]
                                                                          Color:ColorTripPriceText
                                                                       FontSize:10]];
    
    [info appendAttributedString:[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@"%ld天 · ", package.duration]
                                                                          Color:ColorTripTipText
                                                                       FontSize:10]];
    
    [info appendAttributedString:[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@"%@", project.applyPlace]
                                                                          Color:ColorTripTipText
                                                                       FontSize:10]];
    _infoLabel.attributedText = info;
    
//    [_favoButton setSelected:project.hasFavoed];
    
    [_backgroundImageView sd_setImageWithURL:[NSURL URLWithString:[project.imageURLs lastObject]]];
}

@end
