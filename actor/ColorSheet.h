//
//  ColorSheet.h
//  maruko
//
//  Created by 王澍宇 on 16/2/21.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#ifndef ColorSheet_h
#define ColorSheet_h

#import <EDColor.h>

#define ColorNormalNaviTitle        [UIColor colorWithHex:0x474747]
#define ColorNormalSearchBar        [UIColor colorWithHex:0xEEEEEE]
#define ColorNormalButtonTitle      [UIColor colorWithHex:0x474747]
#define ColorNormalBGWhite          [UIColor colorWithHex:0xFFFFFF]
#define ColorNormalTabLine          [UIColor colorWithHex:0xD8D8D8]
#define ColorNormalPlaceholder      [UIColor colorWithHex:0xE1E1E1]
#define ColorNormalNaviItem         [UIColor colorWithHex:0x00BBFF]

#define ColorTabTitle               [UIColor colorWithHex:0x4A4A4A]
#define ColorTabTitle_H             [UIColor colorWithHex:0x00BBFF]

#define ColorTextContent            [UIColor colorWithHex:0x474747]

#define ColorCellLine               [UIColor colorWithHex:0xE1E1E1]
#define ColorCellText               [UIColor colorWithHex:0x4A4A4A]
#define ColorCellPlaceholder        [UIColor colorWithHex:0x999999]
#define ColorCellVecLine            [UIColor colorWithHex:0xD8D8D8]

#define ColorTableSection           [UIColor colorWithHex:0xF4F4F4]

#define ColorWelcomeTextMain        [UIColor colorWithHex:0xFFFFFF]
#define ColorWelcomeTextPlaceholer  [UIColor colorWithHex:0xDFDFDF]
#define ColorWelcomeWechat          [UIColor colorWithHex:0x11CD6E]
#define ColorWelcomeWeibo           [UIColor colorWithHex:0x00BBFF]

#define ColorAccountPlacehodler     [UIColor colorWithHex:0xC1C1C1]
#define ColorAccountTextMain        [UIColor colorWithHex:0xFFFFFF]
#define ColorAccountSMSBG           [UIColor colorWithHex:0x00BBFF]
#define ColorAccountBlack           [UIColor colorWithHex:0x000000]

#define ColorUserCellBG             [UIColor colorWithHex:0x00BBFF]

#define ColorTopicNaviBG            [UIColor colorWithHex:0x00BBFF]
#define ColorTopicTipText           [UIColor colorWithHex:0x00BBFF]
#define ColorTopicText              [UIColor colorWithHex:0x4A4A4A]

#define ColorTripPriceText          [UIColor colorWithHex:0x00BBFF]
#define ColorTripTipText            [UIColor colorWithHex:0x9B9B9B]
#define ColorTripNaviLine           [UIColor colorWithHex:0x979797]
#define ColorTripLocationButtonBG   [UIColor colorWithHex:0x00BBFF]

#define ColorOrderText              [UIColor colorWithHex:0x4A4A4A]
#define ColorOrderStatusText        [UIColor colorWithHex:0x00BBFF]
#define ColorOrderPlacehodler       [UIColor colorWithHex:0x979797]

#define ColorProjectFeeLabelBG      [UIColor colorWithHex:0x000000]
#define ColorProjectApplyLabelBG    [UIColor colorWithHex:0x00BBFF]
#define ColorProjectTextLabelBG     [UIColor colorWithHex:0x000000]
#define ColorProjectlTipText        [UIColor colorWithHex:0x00BBFF]
#define ColorProjectMoreButton      [UIColor colorWithHex:0x00BBFF]
#define ColorProjectShadowBG        [UIColor colorWithHex:0x000000]
#define ColorProjectPlaceText       [UIColor colorWithHex:0x9B9B9B]
#define ColorProjectPolicy          [UIColor colorWithHex:0x4A90E2]
#define ColorProjectBlack           [UIColor colorWithHex:0x000000]

#define ColorCredentialBlack        [UIColor colorWithHex:0x000000]
#define ColorCredentialBlue         [UIColor colorWithHex:0x4A90E2]
#define ColorCredentialRed          [UIColor colorWithHex:0x00BBFF]
#define ColorCredentialLightRed     [UIColor colorWithHex:0x67D7FF]

#endif /* ColorSheet_h */
