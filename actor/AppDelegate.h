//
//  AppDelegate.h
//  actor
//
//  Created by 王澍宇 on 16/3/15.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVOSCloudIM/AVOSCloudIM.h>
#import <AVOSCloud/AVOSCloud.h>
#import <AFNetworking.h>
#import <WeiboSDK.h>
#import <Pingpp.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

