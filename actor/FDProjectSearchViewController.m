//
//  FDProjectSearchViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/10.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectSearchViewController.h"

@implementation FDProjectSearchViewController {
    
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadSegmentControl];
    
}

- (void)loadSegmentControl {
    
    WeakSelf;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    view.backgroundColor = ColorOrderStatusText;
    
    self.navigationItem.rightBarButtonItem.tintColor = ColorNormalBGWhite;
    
    self.searchBar.layer.borderColor = ColorCellLine.CGColor;
    self.searchBar.layer.borderWidth = 0.5;
    
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"默认排序", @"价格从高到底", @"价格从低到高"]];
    segmentedControl.frame = CGRectMake(20, 2, SCREEN_WIDTH - 40, 28);
    segmentedControl.tintColor = ColorNormalBGWhite;
    segmentedControl.selectedSegmentIndex = 0;
    segmentedControl.segmentedAction = ^(UISegmentedControl *segmentedControl) {
        
        StrongSelf;
        
        [s_self.viewModel.prams setObject:@(segmentedControl.selectedSegmentIndex) forKey:@"sort_type"];
        
        [s_self.tableView.mj_header beginRefreshing];
        
    };
    
    [view addSubview:segmentedControl];
    
    CGRect frame = self.tableView.frame;
    frame.origin.y += 35;
    
    self.tableView.frame = frame;
    
    [self.view addSubview:view];
    
}



@end
