//
//  FDBaseViewController.m
//  maruko
//
//  Created by 王澍宇 on 16/2/21.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDBaseViewController.h"

@implementation FDBaseViewController

- (instancetype)init {
    if (self = [super init]) {
        self.window = [UIApplication sharedApplication].keyWindow;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBarItem];
    [self setupNavigationControllerConfig];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNaviBarType:FDNaviBarTypeRed];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.navigationController isKindOfClass:[FDNavigationController class]]) {
        [(FDNavigationController *)self.navigationController showTabBarIfNeed];
    }
}

- (void)setupBarItem {
    UIBarButtonItem *leftItem = nil;
    
    if ([self.navigationController.viewControllers count] > 1) {
        leftItem = [self generateBarItemWithImage:FDImageWithName(@"Normal_Back") SEL:@selector(goBack)];
    } else {
        if (self.presentingViewController) {
            leftItem = [self generateBarItemWithImage:FDImageWithName(@"Normal_Cancel") SEL:@selector(dismiss)];
        }
    }
    
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)setupNavigationControllerConfig {
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    id<UIGestureRecognizerDelegate> target = self.navigationController.interactivePopGestureRecognizer.delegate;
    
    self.backGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:target action:NSSelectorFromString(@"handleNavigationTransition:")];
    self.backGestureRecognizer.delegate = self;
    
    [self.view addGestureRecognizer:self.backGestureRecognizer];
    [self.view setUserInteractionEnabled:YES];
    [self.view setBackgroundColor:ColorNormalBGWhite];
    
    if (self.navigationController.navigationBar.isHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

- (void)updateNaviBarType:(FDNaviBarType)type {
    
    UIBarStyle barStyle = UIBarStyleBlack;
    
    UIImage *backgroundImage = nil;
    
    UIImage *shadowImage = [UIImage new];
    
    UIColor *tintColor = nil;
    
    BOOL translucent = YES;
    
    if (type == FDNaviBarTypeRed) {
        barStyle = UIBarStyleBlack;
        tintColor = ColorNormalBGWhite;
        translucent = NO;
        backgroundImage = [UIImage imageWithColor:ColorTopicNaviBG];
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorNormalBGWhite,
                                                                          NSFontAttributeName : [UIFont systemFontOfSize:20 weight:UIFontWeightRegular]}];
        
    } else if (type == FDNaviBarTypeWhite) {
        
        barStyle = UIBarStyleDefault;
        tintColor = ColorNormalNaviItem;
        translucent = NO;
        backgroundImage = [UIImage imageWithColor:ColorNormalBGWhite];
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorNormalNaviTitle,
                                                                          NSFontAttributeName : [UIFont systemFontOfSize:20 weight:UIFontWeightRegular]}];
        
    } else {
        barStyle = UIBarStyleBlack;
        tintColor = ColorNormalBGWhite;
        translucent = YES;
        backgroundImage = [UIImage new];
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorNormalBGWhite,
                                                                          NSFontAttributeName : [UIFont systemFontOfSize:20 weight:UIFontWeightRegular]}];
    }
    
    [self.navigationController.navigationBar setBarStyle:barStyle];
    [self.navigationController.navigationBar setTranslucent:translucent];
    [self.navigationController.navigationBar setShadowImage:shadowImage];
    [self.navigationController.navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationItem.leftBarButtonItem setTintColor:tintColor];
}

- (FDBarButtonItem *)generateBarItemWithImage:(UIImage *)image SEL:(SEL)selector {
    FDBarButtonItem *item = [[FDBarButtonItem alloc] initWithImage:image
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:selector];
    item.tintColor = ColorNormalNaviItem;
    
    return item;
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return self.navigationController.viewControllers.count == 1 ? NO : YES;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end
