//
//  FDInfoView.m
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDInfoView.h"

@implementation FDInfoView {
    BOOL _isMoved;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    
    if (self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)]) {

        self.info = info;
        
        _shadowView = [[UIImageView alloc] initWithImage:[UIImage imageWithColor:color]];
        _shadowView.frame = self.bounds;
        _shadowView.userInteractionEnabled = YES;
        
        _contentView = [UIView new];
        _contentView.backgroundColor = ColorNormalBGWhite;
        _contentView.layer.cornerRadius = 4.0;
        
        WeakSelf;
        
        _cancelButton = [FDButton buttonWithType:UIButtonTypeCustom ActionBlock:^(FDButton *button) {
            
            StrongSelf;
        
            [s_self dismiss];
            
        }];
        
        [_cancelButton setImage:FDImageWithName(@"Project_Cancel") forState:UIControlStateNormal];
        
        _titleLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:18 Alignment:NSTextAlignmentLeft Light:YES];
        
        _firstLine = [UIView new];
        _firstLine.backgroundColor = ColorCellText;
        
        [self addSubview:_shadowView];
        
        [_shadowView addSubview:_contentView];
        
        [_contentView addSubview:_cancelButton];
        
        [_contentView addSubview:_titleLabel];
        
        [_contentView addSubview:_firstLine];
        
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.centerY.equalTo(@0);
        }];
        
        [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-5));
            make.top.equalTo(@(5));
        }];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(@20);
        }];
        
        [_firstLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleLabel);
            make.top.equalTo(_titleLabel.mas_bottom).offset(10);
            make.size.mas_equalTo(CGSizeMake(30, 0.5));
        }];
        
    }
    return self;
}

- (void)showWithView:(UIView *)view Duration:(NSTimeInterval)duration {
    
    [self setAlpha:0.0];
    
    [view addSubview:self];
    
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 1.0;
    }];
}

- (void)showWithView:(UIView *)view {
    
    [self showWithView:view Duration:0.25];
}

- (void)dismiss {
    
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}

- (void)moveHeigher {
    
    if (_isMoved) {
        return;
    }
    
    _isMoved = YES;
    
    CGRect frame = self.contentView.frame;
    frame.origin.y -= 70;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.frame = frame;
    }];
    
}

- (void)moveLower {
    
    if (!_isMoved) {
        return;
    }
    
    _isMoved = NO;
    
    
    CGRect frame = self.contentView.frame;
    frame.origin.y += 70;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.frame = frame;
    }];
}

@end
