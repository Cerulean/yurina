//
//  FDProjectDateView.m
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectDateView.h"

@implementation FDProjectDateView {
    UILabel *_tipLabel;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    if (self = [super initWithShadowColor:color Info:info]) {
        
        self.titleLabel.text = @"商品库存";
        
        _tipLabel = [UILabel labelWithText:@"义卖周期与商品库存" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        [self.contentView addSubview:_tipLabel];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(self.firstLine.mas_bottom).offset(30);
        }];
        
        UIScrollView *scrollView = [UIScrollView new];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator   = NO;
        
        for (NSInteger index = 0; index < [self.info count]; ++index) {
            
            FDDepartureTime *time = self.info[index];
            
            NSDateComponents *components = [FDDateFormatter dateComponentsFromDate:time.startTime];
            
            NSString *infoString = [NSString stringWithFormat:@"%@ %ld年%ld月%ld日 剩余商品 %ld 个", time.period, components.year, components.month, components.day, time.vacancy];
            
            FDButton *timeButton = [FDButton buttonWithType:UIButtonTypeCustom Title:infoString FontSize:10 ActionBlock:^(FDButton *button) {
                
            }];
            
            [timeButton.layer setBorderColor:ColorCellText.CGColor];
            [timeButton.layer setBorderWidth:0.5];
            [timeButton.layer setCornerRadius:2.0];
            [timeButton setFrame:CGRectMake(20, (30 + 10) * index, SCREEN_WIDTH - 40 * 2, 30)];
            [timeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [timeButton setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
            
            [scrollView addSubview:timeButton];
        }
        
        scrollView.contentSize = CGSizeMake(SCREEN_WIDTH - 35 * 2, 40 * [self.info count] + 30);
        
        [self.contentView addSubview:scrollView];
        
        CGFloat contentHeight = scrollView.contentSize.height;
        
        [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(0));
            make.right.equalTo(@(0));
            make.height.equalTo(contentHeight > SCREEN_HEIGHT / 5 ? @(SCREEN_HEIGHT / 5) : @(contentHeight));
            make.top.equalTo(_tipLabel.mas_bottom).offset(10);
            make.bottom.equalTo(@(-30));
        }];
        
    }
    return self;
}

@end
