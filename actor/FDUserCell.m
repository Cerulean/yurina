//
//  FDUserCell.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDUserCell.h"

@implementation FDUserCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _avatar = [UIImageView new];
        _avatar.clipsToBounds = YES;
        _avatar.backgroundColor = ColorNormalPlaceholder;
        _avatar.layer.borderColor = ColorNormalBGWhite.CGColor;
        _avatar.layer.borderWidth = 0.5;
        _avatar.layer.cornerRadius = 30;
        
        _nameLabel = [UILabel labelWithText:@"用户名" Color:ColorNormalBGWhite FontSize:18 Alignment:NSTextAlignmentLeft Light:NO];
        
//        _infoButton = [UILabel labelWithText:@"完善个人资料" Color:ColorNormalBGWhite FontSize:10 Alignment:NSTextAlignmentCenter Light:NO];
//        [_infoButton.layer setBorderColor:ColorNormalBGWhite.CGColor];
//        [_infoButton.layer setBorderWidth:0.5];
//        [_infoButton.layer setCornerRadius:10.0];
//        
        ContentViewAddSubView(_avatar);
        ContentViewAddSubView(_nameLabel);
//        ContentViewAddSubView(_infoButton);
        
        self.arrow.hidden = NO;
        self.line.hidden  = YES;
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(@20);
            make.size.mas_equalTo(CGSizeMake(60, 60));
        }];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_avatar.mas_right).offset(10);
            make.centerY.equalTo(_avatar);
        }];
        
//        [_infoButton mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.right.equalTo(self.arrow.mas_left).offset(-30);
//            make.centerY.equalTo(_avatar);
//            make.width.equalTo(@70);
//            make.height.equalTo(@(20));
//        }];
        
        self.contentView.backgroundColor = ColorUserCellBG;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

@end
