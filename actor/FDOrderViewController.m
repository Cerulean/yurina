//
//  FDOrderViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/2.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDOrderViewController.h"

#import "FDCredentialViewController.h"
#import "FDDefaultInfoView.h"
#import "FDOrder.h"

@implementation FDOrderViewController {
    FDDefaultInfoView *_infoView;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadNotificationCallback];
    
    [self configureNaviBar];

}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.tableView.mj_header beginRefreshing];
}


- (void)loadNotificationCallback {
    
    WeakSelf;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kCheckoutSuccessNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      
                                                      StrongSelf;
                                                      
                                                      [s_self.tableView.mj_header beginRefreshing];
        
    }];
    
    self.upRefreshCallback = ^(NSArray *objects, FDBaseModel *model, NSError *error) {
        
        StrongSelf;
        
        if (!objects.count) {
            
            [s_self loadPlaceholdViewIfNeed];
            
        } else {
            
            [s_self->_infoView dismiss];
        }
        
    };
}

- (void)configureNaviBar {
    
    WeakSelf;
    
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"正在进行", @"过往订单"]];
    segmentedControl.frame = CGRectMake(0, 0, SCREEN_WIDTH - 20, 30);
    segmentedControl.tintColor = ColorNormalBGWhite;
    segmentedControl.selectedSegmentIndex = 0;
    segmentedControl.segmentedAction = ^(UISegmentedControl *segmentedControl) {
        
        StrongSelf;
        
        s_self.viewModel.prams[@"order_status"] = @(segmentedControl.selectedSegmentIndex + 1);
        
        [s_self.tableView.mj_header beginRefreshing];
        
    };
    
    self.navigationItem.titleView = segmentedControl;
    
}

- (void)loadPlaceholdViewIfNeed {
    
    if (!_infoView) {
        
        _infoView = [[FDDefaultInfoView alloc] initWithHeadText:@"完成您的第一单义卖"
                                                        TipText:@"让世界温暖一点"
                                                          Image:FDImageWithName(@"Normal_Placeholder")
                                                BackgounrdImage:FDImageWithName(@"Order_Trip_BG")];
    }
    
    [_infoView showWithView:self.navigationController.view Duration:0.0];
    [self.navigationController.view bringSubviewToFront:self.navigationController.navigationBar];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    FDOrder *order = self.viewModel.objects[indexPath.row];
    
    if (order.status < FDOrderStatusNeedReview || order.status > FDOrderStatusProjectFinished) {
        return;
    }
    
    FDCredentialViewController *credentialController = [FDCredentialViewController new];
    credentialController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"order/credential"];
    credentialController.viewModel.prams[@"order_id"] = order.objectID;
    
    [self.navigationController pushViewController:credentialController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170;
}

@end
