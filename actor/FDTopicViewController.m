//
//  FDTopicViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/18.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDTopicViewController.h"
#import "FDTopicDetailViewController.h"
#import "FDSearchViewController.h"
#import "FDSaleService.h"
#import "FDAdsView.h"

@interface FDTopicViewController ()

@end

@implementation FDTopicViewController {
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self configureNaviBar];
    
    [self loadAdsView];
}

- (void)configureNaviBar {
    
    WeakSelf;
    
    UIView *searchBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30, 30)];
    searchBar.tapAction = ^(UIView *view) {
        StrongSelf;
        
        FDSearchViewController *searchViewController = [FDSearchViewController new];
        searchViewController.rowHeight = 260;
        searchViewController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"search/topic" CellClass:[FDTopicCell class]];
        
        FDNavigationController *naviController = [[FDNavigationController alloc] initWithRootViewController:searchViewController];
        naviController.transitioningDelegate = naviController;
        
        [s_self presentViewController:naviController animated:YES completion:nil];
    };
    
    UIImageView *icon = [[UIImageView alloc] initWithImage:FDImageWithName(@"Topic_Search")];
    
    UILabel *tipLabel = [UILabel labelWithText:@"搜索专题" Color:ColorTopicTipText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
    
    [searchBar addSubview:icon];
    [searchBar addSubview:tipLabel];
    
    searchBar.backgroundColor = ColorNormalBGWhite;
    searchBar.layer.cornerRadius = 4.0;
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.centerY.equalTo(@0);
    }];
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right).offset(5);
        make.centerY.equalTo(@0);
    }];
    
    self.navigationItem.titleView = searchBar;
}

- (void)loadAdsView {
    
    [FDSaleService getAdsWithCallback:^(FDAds *ads, BOOL success) {
        
        if (success && ads.imageURL) {
            
//            FDAdsView *adsView = [[FDAdsView alloc] initWithShadowColor:[UIColor clearColor] Info:ads];
            
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
//                [adsView showWithView:self.navigationController.view];
            
//            });
        }
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 260;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    FDTopic *topic = self.viewModel.objects[indexPath.row];
    
    FDBaseViewModel *viewModel = [[FDBaseViewModel alloc] initWithAPI:@"project/topic" CellClass:[FDProjectCell class]];
    viewModel.prams[@"topic_id"] = topic.objectID;
    
    FDTopicDetailViewController *topicDetailViewController = [FDTopicDetailViewController new];
    topicDetailViewController.title = topic.title;
    topicDetailViewController.viewModel = viewModel;
    topicDetailViewController.enableUpRefresh = NO;
    topicDetailViewController.enableDownRefresh = NO;
    
    [self.navigationController pushViewController:topicDetailViewController animated:YES];
}

@end
