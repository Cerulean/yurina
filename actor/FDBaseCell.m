//
//  FDBaseCell.m
//  maruko
//
//  Created by 王澍宇 on 16/2/21.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDBaseCell.h"

@implementation FDBaseCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _line = [UIView new];
        _line.backgroundColor = ColorCellLine;
        
        _arrow = [[UIImageView alloc] initWithImage:FDImageWithName(@"Account_Arrow_White")];
        
        [self.contentView addSubview:_line];
        [self.contentView addSubview:_arrow];
        
        [_arrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-10));
            make.centerY.equalTo(@0);
        }];
        
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.bottom.right.equalTo(@0);
            make.height.equalTo(@0.5);
        }];
        
        _arrow.hidden = YES;
    }
    return self;
}

- (void)bindWithModel:(FDBaseModel *)model {
    
}


@end
