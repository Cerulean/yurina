//
//  FDPerson.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDPerson.h"

@implementation FDPerson

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name"     : @"name",
             @"tel"      : @"tel",
             @"email"    : @"email",
             @"address"  : @"address"
             };
}

@end
