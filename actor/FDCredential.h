//
//  FDCredential.h
//  actor
//
//  Created by 王澍宇 on 16/4/5.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"
#import "FDOrder.h"

@interface FDCredential : FDBaseModel

@property (nonatomic, strong) NSString *personName;

@property (nonatomic, strong) NSString *personTel;

@property (nonatomic, strong) NSString *personEmail;

@property (nonatomic, strong) NSString *personAddress;

@property (nonatomic, assign) NSInteger count;

@property (nonatomic, strong) NSString *packageName;

@property (nonatomic, strong) NSString *period;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *channel;

@property (nonatomic, strong) NSString *orderID;

@property (nonatomic, strong) NSDate *orderCreateDate;

@property (nonatomic, assign) FDOrderStatus orderStatus;

@property (nonatomic, assign) float applyFee;

@property (nonatomic, assign) float projectFee;

@end
