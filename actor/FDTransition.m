//
//  FDTransition.m
//  actor
//
//  Created by 王澍宇 on 16/3/30.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDTransition.h"

@implementation FDTransition

+ (instancetype)transitionWithType:(FDTransitionType)type {
    FDTransition *transition = [self new];
    transition.type = type;
    return transition;
}

- (void)presentAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{

    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    toViewController.view.alpha = 0.0;
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        toViewController.view.alpha = 1.0;
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
}

- (void)dismissAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        fromViewController.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
        [fromViewController.view removeFromSuperview];
    }];
}


#pragma mark - UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    switch (_type) {
        case FDTransitionTypePresent:
            [self presentAnimation:transitionContext];
            break;
        case FDTransitionTypeDismiss:
            [self dismissAnimation:transitionContext];
            break;
        default:
            break;
    }
}

@end
