//
//  FDOrder.h
//  actor
//
//  Created by 王澍宇 on 16/4/2.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"
#import "FDDateFormatter.h"

typedef enum : NSUInteger {
    FDOrderStatusNeedPayApplyFee,
    FDOrderStatusNeedReview,
    FDOrderStatusNeedPayProjectFee,
    FDOrderStatusWaitProjectBegin,
    FDOrderStatusProjectProcessing,
    FDOrderStatusProjectFinished,
    FDOrderStatusProjectRefunding,
    FDOrderStatusProjectRefunded,
    FDOrderStatusProjectRejected
} FDOrderStatus;

@interface FDOrder : FDBaseModel

@property (nonatomic, assign) FDOrderStatus status;

@property (nonatomic, strong) NSDate *createDate;

@property (nonatomic, strong) NSDate *departureTime;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *imageURL;

@property (nonatomic, strong) NSString *projectID;

@property (nonatomic, assign) float applyFee;

@property (nonatomic, assign) NSInteger count;

+ (NSString *)stringForStatus:(FDOrderStatus)status;

@end
