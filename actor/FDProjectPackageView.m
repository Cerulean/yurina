//
//  FDProjectPackageView.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectPackageView.h"
#import "FDProjectPeopleView.h"
#import "FDProjectService.h"
#import "FDOrderService.h"

#import "FDCheckoutView.h"

@implementation FDProjectPackageView {
    
    UILabel *_tipLabel;
    
    NSMutableArray *_buttons;
    
    FDButton *_backButton;
    
    FDButton *_confirmButton;
    
    BOOL _canGoNext;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    if (self = [super initWithShadowColor:color Info:info]) {
        
        [self clearPackageInfo];
        
        self.titleLabel.text = @"选择套餐";
        
        _tipLabel = [UILabel labelWithText:@"选择套餐*" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        FDProject *project = (FDProject *)info;
        
        WeakSelf;
        
        _backButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"上一步" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            [s_self clearPackageInfo];
            
            FDProjectPeopleView *peopleView = [[FDProjectPeopleView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9]
                                                                                          Info:s_self.info];
            [s_self dismiss];
            
            [peopleView showWithView:s_self.superview];
            
        }];
        
        [_backButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_backButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        
        _confirmButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"下一步" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            if (!s_self->_canGoNext) {
                return [FDAlert alertWithTitle:@"错误" Message:@"未选择套餐"];
            }
            
            FDCheckoutView *checkoutView = [[FDCheckoutView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9]
                                                                                                Info:s_self.info
                                                                                                Type:FDOrderCheckoutTypeApplyFee];
            
            [s_self dismiss];
            
            [checkoutView showWithView:s_self.superview];
            
        }];
        
        [_confirmButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_confirmButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        
        ContentViewAddSubView(_tipLabel);
        ContentViewAddSubView(_confirmButton);
        ContentViewAddSubView(_backButton);
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(self.firstLine.mas_bottom).offset(30);
        }];
        
        UIScrollView *scrollView = [UIScrollView new];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator   = NO;
        
        _buttons = [NSMutableArray new];
        
        for (NSInteger index = 0; index < [project.packages count]; ++index) {
            
            FDPackage *package = project.packages[index];
            
            NSMutableAttributedString *introString = [[NSAttributedString attributedStringWithString:package.name
                                                                                               Color:ColorCellText
                                                                                            FontSize:10] mutableCopy];
            
            [introString appendAttributedString:[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@"\nCNY %.2f", package.fee / 100]
                                                                                         Color:ColorProjectMoreButton FontSize:10]];
            
//            [introString appendAttributedString:[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@" · %ld天 · %@",
//                                                                                                package.duration,
//                                                                                                project.applyPlace]
//                                                                                         Color:ColorProjectPlaceText FontSize:10]];
            
            NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineSpacing = 5;
            
            [introString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, introString.length)];
            
            FDButton *packageButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"" FontSize:10 ActionBlock:^(FDButton *button) {
                
                StrongSelf;
                
                [s_self updateButtonState:button];
                
                if (button.selected) {
                    
                    NSMutableDictionary *parms = [FDOrderService applyParms];
                    parms[@"package_id"]       = @([package.objectID integerValue]);
                    
                    s_self->_canGoNext = YES;
                    
                } else {
                    
                    s_self->_canGoNext = NO;
                    
                }
            }];
            
            [packageButton setAttributedTitle:introString forState:UIControlStateNormal];
            [packageButton.titleLabel setNumberOfLines:2];
            [packageButton.layer setBorderColor:ColorCellText.CGColor];
            [packageButton.layer setBorderWidth:0.5];
            [packageButton.layer setCornerRadius:2.0];
            [packageButton setFrame:CGRectMake(20, (50 + 10) * index, SCREEN_WIDTH - 40 * 2, 50)];
            [packageButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [packageButton setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
            [packageButton setImageName:@"Project_Checkout_Form" AutoHighlight:NO AutoDisabled:NO AutoSelected:YES];
            [packageButton setImageEdgeInsets:UIEdgeInsetsMake(0, SCREEN_WIDTH - 40 * 2 - 30, 0, 0)];
            
            [scrollView addSubview:packageButton];
            
            [_buttons addObject:packageButton];
        }
        
        scrollView.contentSize = CGSizeMake(SCREEN_WIDTH - 35 * 2, 50 * project.packages.count + 30);
        
        ContentViewAddSubView(scrollView);
        
        CGFloat contentHeight = scrollView.contentSize.height;
        
        [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(0));
            make.right.equalTo(@(0));
            make.height.equalTo(contentHeight > SCREEN_HEIGHT / 5 ? @(SCREEN_HEIGHT / 5) : @(contentHeight));
            make.top.equalTo(_tipLabel.mas_bottom).offset(10);
        }];
        
        [_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(scrollView.mas_bottom).offset(20);
            make.right.equalTo(self.mas_centerX).offset(-10);
            make.height.equalTo(@45);
        }];
        
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_centerX).offset(10);
            make.right.equalTo(@(-20));
            make.top.equalTo(scrollView.mas_bottom).offset(20);
            make.height.equalTo(@45);
            make.bottom.equalTo(@(-20));
        }];
        
    }
    return self;
}

#pragma mark - Helper Method

- (void)clearPackageInfo {
    
    NSMutableDictionary *parms = [FDOrderService applyParms];
    parms[@"package_id"] = nil;
    
}

- (void)updateButtonState:(FDButton *)abutton {
    
    for (FDButton *button in _buttons) {
        
        if (abutton == button) {
            continue;
        }
        
        button.selected = NO;
        
        [button.layer setBorderColor:ColorCellText.CGColor];
    }
    
    [abutton setSelected:!abutton.selected];
    
    [abutton.layer setBorderColor:abutton.selected ? ColorProjectApplyLabelBG.CGColor : ColorCellText.CGColor];
}

@end
