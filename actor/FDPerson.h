//
//  FDPerson.h
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDPerson : FDBaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *tel;

@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) NSString *address;

@end
