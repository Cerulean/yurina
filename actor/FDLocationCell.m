//
//  FDLocationCell.m
//  actor
//
//  Created by 王澍宇 on 16/4/1.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDLocationCell.h"

@implementation FDLocationCell

- (void)bindWithModel:(FDBaseModel *)model {
    
    self.model = model;
    
    if (![self.model isKindOfClass:[FDLocation class]]) {
        return;
    }
    
    FDLocation *location = (FDLocation *)self.model;
    
    self.textLabel.text = location.name;
    
}

@end
