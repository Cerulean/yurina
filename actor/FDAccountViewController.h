//
//  FDAccountViewController.h
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseViewController.h"

typedef enum : NSUInteger {
    FDAccountLogicTypeRegister,
    FDAccountLogicTypeLogin,
    FDAccountLogicTypeBind,
    FDAccountLogicTypeForget,
} FDAccountLogicType;

@interface FDAccountViewController : FDBaseViewController

@property (nonatomic, assign) FDAccountLogicType logicType;

@end
