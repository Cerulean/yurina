//
//  FDSearchViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/29.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDSearchViewController.h"

@implementation FDSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTableView];
    [self configureNaviBar];
}

- (void)configureTableView {
    if (_rowHeight > 0) {
        self.tableView.rowHeight = _rowHeight;
    }
}

- (void)configureNaviBar {
    
    _searchBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 70, 30)];
    
    UIImageView *icon = [[UIImageView alloc] initWithImage:FDImageWithName(@"Topic_Search")];
    
    WeakSelf;
    
    UITextField *textField = [UITextField new];
    textField.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
    textField.textColor = ColorTopicTipText;
    textField.placeholder = @"";
    textField.returnKeyType = UIReturnKeySearch;
    textField.editingAction = ^(NSString *text, UITextField *filed) {
        StrongSelf;
        [s_self.viewModel.prams setObject:text forKey:@"keyword"];
        [s_self.tableView.mj_header beginRefreshing];
    };
    
    [_searchBar addSubview:icon];
    [_searchBar addSubview:textField];
    
    _searchBar.backgroundColor = ColorNormalBGWhite;
    _searchBar.layer.cornerRadius = 4.0;
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.centerY.equalTo(@0);
    }];
    
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right).offset(10);
        make.width.equalTo(@(SCREEN_WIDTH - 80));
        make.centerY.equalTo(@0);
    }];
    
    [textField becomeFirstResponder];
    
    self.navigationItem.rightBarButtonItem = self.navigationItem.leftBarButtonItem;
    self.navigationItem.rightBarButtonItem.tintColor = ColorNormalBGWhite;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_searchBar];
}

@end
