//
//  FDTripViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/19.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectSearchViewController.h"
#import "FDExplorerViewController.h"
#import "FDLocationViewController.h"
#import "FDWebViewController.h"
#import "FDAccountService.h"
#import "FDSaleService.h"

@implementation FDExplorerViewController {
    UIImageView *_avatar;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureNaviBar];
    [self loadHeaderView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([FDAccountService checkIfNeedLogin]) {
        return;
    }
    
    [_avatar sd_setImageWithURL:[NSURL URLWithString:[FDAccountService currentUser].avatarURL]];
}

- (void)loadHeaderView {
    
    self.tableView.backgroundColor = ColorTableSection;
    
    [FDSaleService getBannersWithCallback:^(NSArray<FDBanner *> *banners, BOOL success) {
        
        if (success && banners.count > 0) {
            
            UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];;
            scrollView.showsHorizontalScrollIndicator = NO;
            scrollView.showsVerticalScrollIndicator = NO;
            scrollView.pagingEnabled = YES;
            
            for (NSInteger index = 0; index < banners.count; ++index) {
                
                FDBanner *banner = banners[index];
                
                WeakSelf;
                
                UIImageView *imageView = [UIImageView new];
                [imageView setClipsToBounds:YES];
                [imageView setContentMode:UIViewContentModeScaleAspectFill];
                [imageView sd_setImageWithURL:[NSURL URLWithString:banner.imageURL]];
                [imageView setFrame:CGRectMake(index * SCREEN_WIDTH, 0, SCREEN_WIDTH, 200)];
                [imageView setUserInteractionEnabled:YES];
                [imageView setTapAction:^(UIView *view) {
                    
                    StrongSelf;
                    
                    FDWebViewController *webController = [FDWebViewController new];
                    webController.linkURL = banner.linkURL;
                    
                    [s_self.navigationController pushViewController:webController animated:YES];
                }];
                
                [scrollView addSubview:imageView];
            }
            
            scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * banners.count, 200);
            
            self.tableView.tableHeaderView = scrollView;
        }
        
    }];
    
}

- (void)configureNaviBar {
    
    WeakSelf;
    
    UIView *searchBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30, 30)];
    searchBar.tapAction = ^(UIView *view) {
        
        StrongSelf;
        
        FDProjectSearchViewController *searchViewController = [FDProjectSearchViewController new];
        searchViewController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"search/project" CellClass:[FDProjectCell class]];
        searchViewController.viewModel.prams[@"sort_type"] = @(0);
        searchViewController.rowHeight = 300;
        
        FDNavigationController *naviController = [[FDNavigationController alloc] initWithRootViewController:searchViewController];
        naviController.transitioningDelegate = naviController;
        
        [s_self presentViewController:naviController animated:YES completion:nil];
    };
    
    UIImageView *icon = [[UIImageView alloc] initWithImage:FDImageWithName(@"Topic_Search")];
    
//    FDButton *locationButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"所有" FontSize:10 ActionBlock:^(FDButton *button) {
//        
//    }];
//    
//    [locationButton setImage:FDImageWithName(@"Trip_Down_Arrow") forState:UIControlStateNormal];
//    [locationButton setBackgroundColor:ColorTripLocationButtonBG];
//    [locationButton setFrame:CGRectMake(0, 0, 85, 30)];
//    [locationButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
//    [locationButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
//    [locationButton setContentEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
//    [locationButton setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
//    [locationButton setActionBlock:^(FDButton *button) {
//        StrongSelf;
//        FDLocationViewController *provinceController = [FDLocationViewController new];
//        provinceController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"location/provinces" CellClass:[FDLocationCell class]];
//        provinceController.title = @"选择省";
//        provinceController.callback = ^(id object) {
//            
//            FDLocation *location = (FDLocation *)object;
//            
//            SStrongSelf;
//            
//            [button setTitle:location.name forState:UIControlStateNormal];
//            
//            ss_self.viewModel.prams[@"city_id"] = location.objectID;
//            
//            
//            [ss_self.tableView.mj_header beginRefreshing];
//        };
//        
//        FDNavigationController *naviController = [[FDNavigationController alloc] initWithRootViewController:provinceController];
//        
//        [s_self.navigationController presentViewController:naviController animated:YES completion:nil];
//    }];
    
    UILabel *tipLabel = [UILabel labelWithText:@"搜索义卖、商品" Color:ColorTopicTipText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
    
//    [searchBar addSubview:locationButton];
    [searchBar addSubview:icon];
    [searchBar addSubview:tipLabel];
    
    searchBar.backgroundColor = ColorNormalBGWhite;
    searchBar.layer.cornerRadius = 4.0;
    searchBar.layer.borderWidth = 0.5;
    searchBar.layer.borderColor = ColorNormalBGWhite.CGColor;
    searchBar.clipsToBounds = YES;
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.centerY.equalTo(@0);
    }];
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(icon.mas_right).offset(5);
        make.centerY.equalTo(@0);
    }];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchBar];
    
//    _avatar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [_avatar setBackgroundColor:ColorNormalPlaceholder];
//    [_avatar setFrame:CGRectMake(0, 0, 30, 30)];
//    [_avatar.layer setCornerRadius:15];
//    [_avatar setClipsToBounds:YES];
//    [_avatar sd_setImageWithURL:[NSURL URLWithString:[FDAccountService currentUser].avatarURL]];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_avatar];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300;
}

@end
