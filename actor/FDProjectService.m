//
//  FDProjectWebService.m
//  actor
//
//  Created by 王澍宇 on 16/3/23.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectService.h"
#import "FDAccountService.h"

@implementation FDProjectService

+ (void)getProjectWithObjectID:(NSString *)objectID Callback:(void(^)(BOOL success, FDProject *project))callback {
    
    [FDWebService requestWithAPI:@"project/info" Method:@"GET" Parms:@{@"project_id" : objectID} HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (callback) {
            
            callback(success, [MTLJSONAdapter modelOfClass:[FDProject class] fromJSONDictionary:resultDic error:nil]);
            
        }
        
    }];
    
}

+ (void)favoProject:(NSString *)projectID Callback:(FDWebServiceCallback)callback {
    
    if ([FDAccountService checkIfNeedLogin]) {
        return [FDAccountService promptLogin];
    }
    
    [FDWebService requestWithAPI:@"project/favo" Method:@"POST" Parms:@{@"project_id" : projectID} HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            callback(success);
        }
    }];
}

+ (void)unfavoProject:(NSString *)projectID Callback:(FDWebServiceCallback)callback {
    [FDWebService requestWithAPI:@"project/favo" Method:@"DELETE" Parms:@{@"project_id" : projectID} HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            callback(success);
        }
    }];
}

+ (void)consultWithName:(NSString *)name tel:(NSString *)tel mail:(NSString *)mail problem:(NSString *)problem Callback:(FDWebServiceCallback)callback {
    
    if (![FDValidater validateEmail:mail]) {
        return [FDAlert alertWithTitle:@"错误" Message:@"邮箱不合法"];
    }
    
    if (!problem.length) {
        return [FDAlert alertWithTitle:@"错误" Message:@"反馈内容不能为空"];
    }
    
    if (!name.length) {
        return [FDAlert alertWithTitle:@"错误" Message:@"姓名不能为空"];
    }
    
    [FDWebService requestWithAPI:@"project/consult" Method:@"POST" Parms:@{
                                                                           @"name"    : name,
                                                                           @"tel"     : tel,
                                                                           @"email"   : mail,
                                                                           @"problem" : problem
                                                                           }
                             HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
                                 if (callback) {
                                     callback(success);
                                 }
                             }];
}

@end
