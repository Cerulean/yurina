//
//  FDOrderCell.h
//  actor
//
//  Created by 王澍宇 on 16/3/19.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import <MJRefresh/MJRefresh.h>

#import "FDBaseCell.h"
#import "FDOrder.h"

@interface FDOrderCell : FDBaseCell

PropertyStrong UILabel *numberLabel;

PropertyStrong UIImageView *icon;

PropertyStrong UILabel *titleLabel;

PropertyStrong UILabel *startDateLabel;

PropertyStrong UIView *horLine;

PropertyStrong UILabel *statusLabel;

PropertyStrong FDButton *button;

PropertyStrong UIImageView *statusImageView;

PropertyStrong UILabel *feeLabel;

@end
