//
//  FDModifyViewController.h
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseStaticCellController.h"

@interface FDModifyViewController : FDBaseStaticCellController

@property (nonatomic, strong) NSString *key;

@property (nonatomic, strong) NSString *prompt;

@property (nonatomic, strong) UITextField *textField;

@end
