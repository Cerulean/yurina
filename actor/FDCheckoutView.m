//
//  FDProjectCheckoutView.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDCheckoutView.h"
#import "FDProjectPeopleView.h"
#import "FDWebViewController.h"
#import "FDCredentialViewController.h"

@implementation FDCheckoutView {
    
    UILabel *_projectLabel;
    
    UILabel *_introLabel;
    
    UILabel *_nameLabel;
    
    UILabel *_telLabel;
    
    UILabel *_mailLabel;
    
    UILabel *_addressLabel;
    
    UILabel *_countLabel;
    
    FDButton *_modifyButton;
    
    UIView *_secondLine;
    
    UILabel *_tipLabel;
    
    NSMutableArray *_buttons;
    
    UIView *_thirdLine;
    
    UILabel *_applyFeeLabel;
    
    FDButton *_policyButton;
    
    FDButton *_policyIcon;
    
    FDButton *_confirmButton;
    
    UIImageView *_icon;
    
    UILabel *_agreementsLabel;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info Type:(FDOrderCheckoutType)type {
    if (self = [super initWithShadowColor:color Info:info]) {
        
        self.titleLabel.text = @"结算";
        
        FDCheckout *checkout = nil;
        
        if (type == FDOrderCheckoutTypeProjectFee) {
            
            checkout = (FDCheckout *)self.info;
            checkout.projectFee /= 100;
            checkout.fee /= 100;
            
            [FDOrderService applyParms][@"order_id"] = checkout.orderID;
            
        } else {
            
            FDProject *project = (FDProject *)info;
            
            checkout = [FDCheckout new];
            
            checkout.title = project.title;
            
            FDPackage *package = nil;
            
            NSString *packageID = [[FDOrderService applyParms][@"package_id"] stringValue];
            
            for (FDPackage *_package in project.packages) {
                
                if ([_package.objectID isEqualToString:packageID]) {
                    
                    package = _package;
                    
                    break;
                }
            }
            
            FDDepartureTime *time = nil;
            
            NSString *departureTimeID = [[FDOrderService applyParms][@"departure_time_id"] stringValue];
            
            for (FDDepartureTime *_time in project.departureTimes) {
                
                if ([_time.objectID isEqualToString:departureTimeID]) {
                    
                    time = _time;
                    
                    break;
                }
                
            }
            
            FDPerson *person = [MTLJSONAdapter modelOfClass:[FDPerson class]
                                         fromJSONDictionary:[[FDOrderService applyParms][@"people"] firstObject]
                                                      error:nil];
            
            NSInteger peopleCount = [[FDOrderService applyParms][@"people"] count];
            
            CGFloat fee = type == FDOrderCheckoutTypeApplyFee ? project.applyFee : package.fee;
            fee *= peopleCount;
            fee /= 100;
            
            checkout.departureTime = time.startTime;
            checkout.period        = time.period;
            checkout.vacancy       = time.vacancy;
            checkout.projectFee    = package.fee / 100;
            checkout.personName    = person.name;
            checkout.personTel     = person.tel;
            checkout.personEmail   = person.email;
            checkout.personAddress = person.address;
            checkout.personCount   = peopleCount;
            checkout.fee           = fee;
            checkout.cancelIntro   = project.cancelIntro;
        }
        
        _projectLabel = [UILabel labelWithText:checkout.title Color:ColorCellText FontSize:12 Alignment:NSTextAlignmentLeft Light:NO];
        _projectLabel.numberOfLines = 2;
        
        
        NSDateComponents *components = [FDDateFormatter dateComponentsFromDate:checkout.departureTime];
        
        NSString *infoString = [NSString stringWithFormat:@"%@ %ld年%ld月%ld日 剩余件数 %ld 个 · %ld 件", checkout.period, components.year, components.month, components.day, checkout.vacancy, checkout.personCount];
        
        NSMutableAttributedString *introString = [[NSAttributedString attributedStringWithString:infoString
                                                                                           Color:ColorCellText
                                                                                        FontSize:10] mutableCopy];
        
//        [introString appendAttributedString:[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@" · CNY %.2f", checkout.projectFee]
//                                                                                     Color:ColorCellText FontSize:14]];
//        
        _introLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:NO];
        _introLabel.attributedText = introString;
        
        _nameLabel = [UILabel labelWithText:[NSString stringWithFormat:@"收货人：%@", checkout.personName]
                                      Color:ColorCellText
                                   FontSize:10
                                  Alignment:NSTextAlignmentLeft
                                      Light:YES];
        
        _telLabel = [UILabel labelWithText:[NSString stringWithFormat:@"电话：%@", checkout.personTel]
                                     Color:ColorCellText FontSize:10
                                 Alignment:NSTextAlignmentLeft Light:YES];
        
        _mailLabel = [UILabel labelWithText:[NSString stringWithFormat:@"电子邮箱：%@", checkout.personEmail]
                                      Color:ColorCellText
                                   FontSize:10
                                  Alignment:NSTextAlignmentLeft
                                      Light:YES];
        
        _addressLabel = [UILabel labelWithText:[NSString stringWithFormat:@"收货地址：%@", checkout.personAddress]
                                         Color:ColorCellText
                                      FontSize:10
                                     Alignment:NSTextAlignmentLeft
                                         Light:YES];
        
        WeakSelf;
        
        _modifyButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"返回修改" FontSize:10 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            FDProjectPeopleView *peopleView = [[FDProjectPeopleView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9]
                                                                                          Info:s_self.info];
            
            [s_self dismiss];
            
            [peopleView showWithView:s_self.superview];
            
        }];
        
        [_modifyButton setTitleColor:ColorProjectlTipText forState:UIControlStateNormal];
        [_modifyButton setEnabled:type == FDOrderStatusNeedPayApplyFee ? YES : NO];
        
        _secondLine = [UIView new];
        _secondLine.backgroundColor = ColorCellText;
        
        _tipLabel = [UILabel labelWithText:@"选择支付方式" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _countLabel = [UILabel labelWithText:@"支付金额" Color:ColorProjectlTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _thirdLine = [UIView new];
        _thirdLine.backgroundColor = ColorCellText;
        
        NSString *info = @"共计";
        
        _applyFeeLabel = [UILabel labelWithText:[NSString stringWithFormat:@"%@：CNY %.2f", info, checkout.fee]
                                          Color:ColorProjectlTipText
                                       FontSize:14
                                      Alignment:NSTextAlignmentLeft
                                          Light:YES];
        
        _policyIcon = [FDButton buttonWithType:UIButtonTypeCustom ActionBlock:^(FDButton *button) {
            [button setSelected:!button.selected];
        }];
        
        [_policyIcon setImageName:@"Project_Policy" AutoHighlight:NO AutoDisabled:NO AutoSelected:YES];
        [_policyIcon setSelected:YES];
        [_policyIcon setHidden:YES];
        
        CGRect frame = _policyIcon.imageView.frame;
        frame.size = CGSizeMake(10, 10);
        
        _policyIcon.imageView.frame = frame;
        
        _policyButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"《退订政策》" FontSize:12 ActionBlock:^(FDButton *button) {
            FDWebViewController *webController = [FDWebViewController new];
            webController.htmlString = checkout.cancelIntro;
            
            [UIWindow presentViewController:webController Animated:YES Completion:nil];
        }];
        
        [_policyButton setTitleColor:ColorProjectPolicy forState:UIControlStateNormal];
        [_policyButton setHidden:YES];
        
        _confirmButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"确认支付" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            if (!s_self->_policyIcon.selected) {
                return [FDAlert alertWithTitle:@"错误" Message:@"需要同意我们的《退订政策》才能进行结算"];
            }
            
            if (![FDOrderService applyParms][@"channel"]) {
                [FDOrderService applyParms][@"channel"] = kChannelWechat;
            }
            
            [FDOrderService checkoutFee:checkout.fee Type:type WithCallback:^(BOOL success, NSString *orderID) {
                
                if (success) {
                    
                    [s_self dismiss];
                    
                    FDCredentialViewController *credentialController = [FDCredentialViewController new];
                    credentialController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"order/credential"];
                    credentialController.viewModel.prams[@"order_id"] = orderID;
                    
                    [UIWindow presentViewController:credentialController Animated:YES Completion:nil];
                    
                }
                
            }];
            
        }];
        
        [_confirmButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_confirmButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:YES AutoSelected:NO];
        
        _icon = [[UIImageView alloc] initWithImage:FDImageWithName(@"Project_Agreements")];
        
        _agreementsLabel = [UILabel labelWithText:@"" Color:ColorProjectPolicy FontSize:10 Alignment:NSTextAlignmentCenter Light:YES];
        
        introString = [[NSAttributedString attributedStringWithString:@"我已经阅读并同意"
                                                                Color:ColorProjectPlaceText
                                                                FontSize:10] mutableCopy];
        
        [introString appendAttributedString:[NSAttributedString attributedStringWithString:@"企鹅 · 义卖服务使用协议"
                                                                                     Color:ColorProjectPolicy
                                                                                  FontSize:10]];
        
        _agreementsLabel.attributedText = introString;
        _agreementsLabel.userInteractionEnabled = YES;
        _agreementsLabel.tapAction = ^(UIView *view) {
            
            FDWebViewController *webController = [FDWebViewController new];
            
            webController.linkURL = kURLDuty;
            
            [UIWindow presentViewController:webController Animated:YES Completion:nil];
        };
        
        ContentViewAddSubView(_projectLabel);
        ContentViewAddSubView(_introLabel);
        ContentViewAddSubView(_nameLabel);
        ContentViewAddSubView(_telLabel);
        ContentViewAddSubView(_mailLabel);
        ContentViewAddSubView(_addressLabel);
        ContentViewAddSubView(_modifyButton);
        ContentViewAddSubView(_secondLine);
        ContentViewAddSubView(_tipLabel);
        ContentViewAddSubView(_countLabel);
        ContentViewAddSubView(_thirdLine);
        ContentViewAddSubView(_applyFeeLabel);
        ContentViewAddSubView(_policyButton);
        ContentViewAddSubView(_policyIcon);
        ContentViewAddSubView(_confirmButton);
        ContentViewAddSubView(_icon);
        ContentViewAddSubView(_agreementsLabel);
        
        [_projectLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.firstLine.mas_bottom).offset(30);
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
        }];
        
        [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.right.equalTo(@(-20));
            make.top.equalTo(_projectLabel.mas_bottom).offset(5);
        }];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_introLabel.mas_bottom).offset(5);
        }];
        
        [_telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_nameLabel.mas_bottom).offset(5);
        }];
        
        [_mailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_telLabel.mas_bottom).offset(5);
        }];
        
        [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_mailLabel.mas_bottom).offset(5);
        }];
        
        [_modifyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_addressLabel.mas_bottom).offset(15);
        }];
        
        [_secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_modifyButton.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
            make.width.equalTo(@30);
        }];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_secondLine.mas_bottom).offset(20);
        }];
        
        UIScrollView *scrollView = [UIScrollView new];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator   = NO;
        
        _buttons = [NSMutableArray new];
        
        for (NSInteger index = 0; index < 2; ++index) {
            
            NSString *buttonTitle = index == 0 ? @"微信支付" : @"支付宝";
            
            FDButton *checkoutButton = [FDButton buttonWithType:UIButtonTypeCustom Title:buttonTitle FontSize:10 ActionBlock:^(FDButton *button) {
                
                StrongSelf;
                
                [s_self updateButtonState:button];
                
                NSMutableDictionary *parms = [FDOrderService applyParms];
                parms[@"channel"] = index == 0 ? kChannelWechat : kChannelAlipay;
                
            }];
            
            [checkoutButton.layer setBorderColor:ColorCellText.CGColor];
            [checkoutButton.layer setBorderWidth:0.5];
            [checkoutButton.layer setCornerRadius:4.0];
            [checkoutButton setFrame:CGRectMake(20, (30 + 10) * index, SCREEN_WIDTH - 40 * 2, 30)];
            [checkoutButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
            [checkoutButton setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
            [checkoutButton setImageName:@"Project_Checkout_Form" AutoHighlight:NO AutoDisabled:NO AutoSelected:YES];
            [checkoutButton setImageEdgeInsets:UIEdgeInsetsMake(0, SCREEN_WIDTH - 40 * 2 - 30, 0, 0)];
            
            NSString *iconName = index == 0 ? @"Project_Checkout_Wexin" : @"Project_Checkout_Alipay";
            
            UIImageView *icon = [[UIImageView alloc] initWithImage:FDImageWithName(iconName)];
            icon.center = CGPointMake(SCREEN_WIDTH / 2 - 40 - 20, 15);
            
            [checkoutButton addSubview:icon];
            
            [scrollView addSubview:checkoutButton];
            
            [_buttons addObject:checkoutButton];
        }
        
        scrollView.contentSize = CGSizeMake(SCREEN_WIDTH - 35 * 2, 40 * [_buttons count] + 30);
        
        ContentViewAddSubView(scrollView);
        
        CGFloat contentHeight = scrollView.contentSize.height;
        
        [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(0));
            make.right.equalTo(@(0));
            make.height.equalTo(contentHeight > SCREEN_HEIGHT / 5 ? @(SCREEN_HEIGHT / 5) : @(contentHeight));
            make.top.equalTo(_tipLabel.mas_bottom).offset(20);
        }];
        
        [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(scrollView.mas_bottom).offset(5);
        }];
        
        [_thirdLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_countLabel.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
            make.width.equalTo(@30);
        }];
        
        [_applyFeeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_projectLabel);
            make.top.equalTo(_thirdLine.mas_bottom).offset(10);
        }];
        
        [_policyIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_applyFeeLabel);
            make.right.equalTo(_policyButton.mas_left).offset(0);
            make.size.mas_equalTo(CGSizeMake(15, 40));
        }];
        
        [_policyButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-20));
            make.centerY.equalTo(_applyFeeLabel);
        }];
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_applyFeeLabel.mas_bottom).offset(20);
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.height.equalTo(@45);
        }];
        
        [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_agreementsLabel.mas_left).offset(-15);
            make.top.equalTo(_confirmButton.mas_bottom).offset(15);
            make.bottom.equalTo(@(-15));
        }];
        
        [_agreementsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_icon);
            make.centerX.equalTo(@0);
        }];
    }
    return self;
}

#pragma mark - Helper Method

- (void)updateButtonState:(FDButton *)abutton {
    
    for (FDButton *button in _buttons) {
        
        if (abutton == button) {
            continue;
        }
        
        button.selected = NO;
        
        [button.layer setBorderColor:ColorCellText.CGColor];
    }
    
    [abutton setSelected:!abutton.selected];
    
    [abutton.layer setBorderColor:abutton.selected ? ColorProjectApplyLabelBG.CGColor : ColorCellText.CGColor];
}

@end
