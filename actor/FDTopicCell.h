//
//  FDTopicCell.h
//  actor
//
//  Created by 王澍宇 on 16/3/18.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseCell.h"
#import "FDTopic.h"

@interface FDTopicCell : FDBaseCell

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *introLabel;

@property (nonatomic, strong) UIImageView *backgroundImageView;

@end
