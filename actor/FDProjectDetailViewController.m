//
//  FDProjectDetailViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/6.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectDetailViewController.h"

@implementation FDProjectDetailViewController {
    
    UIView *_firstLine;
    
    UILabel *_titleLabel;
    
    UIView *_secondLine;
    
    UILabel *_tagLabel;
    
    UIImageView *_locationIcon;
    
    UILabel *_locationLabel;
    
    UIView *_bottomView;
    
    UIWebView *_webView;
    
    UIView *_thirdLine;
    
    FDButton *_backButton;
    
    UILabel *_bottomLabel;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self configureNaviBar];
    
    [self loadSubViews];
    
    [self bindWithProject];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationItem setLeftBarButtonItem:[FDBarButtonItem generateClearItem]];
    [self.navigationItem setRightBarButtonItem:[FDBarButtonItem generateClearItem]];
}

- (void)loadSubViews {
    
    WeakSelf;
    
    self.view.backgroundColor = ColorTableSection;
    
    _firstLine = [UIView new];
    _firstLine.backgroundColor = ColorCellLine;
    _firstLine.hidden = YES;
    
    _titleLabel = [UILabel labelWithText:@"" Color:ColorProjectBlack FontSize:18 Alignment:NSTextAlignmentCenter Light:NO];
    _titleLabel.numberOfLines = 2;
    _titleLabel.hidden = YES;
    
    _secondLine = [UIView new];
    _secondLine.backgroundColor = ColorCellLine;
    _secondLine.hidden = YES;
    
    _tagLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
    _tagLabel.hidden = YES;
    
    _locationIcon  = [[UIImageView alloc] initWithImage:FDImageWithName(@"Project_Location")];
    _locationIcon.hidden = YES;
    
    _locationLabel = [UILabel labelWithText:@"项目地点：" Color:ColorCellText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
    _locationLabel.hidden = YES;
    
    _webView = [UIWebView new];
    _webView.scalesPageToFit = YES;
    _webView.backgroundColor = ColorNormalBGWhite;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    _webView.scrollView.showsHorizontalScrollIndicator = NO;
    
    _thirdLine = [UIView new];
    _thirdLine.backgroundColor = ColorCellLine;
    
    _bottomView = [UIView new];
    _bottomView.backgroundColor = ColorOrderStatusText;
    
    _backButton = [FDButton buttonWithType:UIButtonTypeSystem ActionBlock:^(FDButton *button) {
        
        StrongSelf;
        
        [s_self.navigationController popViewControllerAnimated:YES];
    }];
    
    [_backButton setImage:FDImageWithName(@"Normal_Back") forState:UIControlStateNormal];
    [_backButton setTintColor:ColorNormalBGWhite];
    
    _bottomLabel = [UILabel labelWithText:@"" Color:ColorNormalBGWhite FontSize:18 Alignment:NSTextAlignmentCenter Light:NO];
    
    [self.view addSubview:_firstLine];
    [self.view addSubview:_titleLabel];
    [self.view addSubview:_secondLine];
    [self.view addSubview:_tagLabel];
    [self.view addSubview:_locationIcon];
    [self.view addSubview:_locationLabel];
    [self.view addSubview:_webView];
    [self.view addSubview:_thirdLine];
    [self.view addSubview:_bottomView];
    
    [_bottomView addSubview:_backButton];
    [_bottomView addSubview:_bottomLabel];
    
//    [_firstLine mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(@20);
//        make.right.equalTo(@(-20));
//        make.top.equalTo(@15);
//        make.height.equalTo(@0.5);
//    }];
//    
//    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(_firstLine);
//        make.top.equalTo(_firstLine.mas_bottom).offset(10);
//    }];
//    
//    [_secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(@20);
//        make.right.equalTo(@(-20));
//        make.top.equalTo(_titleLabel.mas_bottom).offset(15);
//        make.height.equalTo(@0.5);
//    }];
//    
//    [_tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_firstLine);
//        make.top.equalTo(_secondLine.mas_bottom).offset(15);
//    }];
//    
//    [_locationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_firstLine);
//        make.top.equalTo(_tagLabel.mas_bottom).offset(20);
//    }];
//    
//    [_locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_locationIcon.mas_right).offset(5);
//        make.centerY.equalTo(_locationIcon);
//    }];
    
    [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.equalTo(@10);
        make.right.equalTo(@(-10));
        make.bottom.equalTo(_bottomView.mas_top);
    }];
    
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(@0);
        make.height.equalTo(@44);
    }];
    
    [_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.centerY.equalTo(_bottomLabel);
    }];
    
    [_bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
        make.width.equalTo(@(SCREEN_WIDTH - 100));
    }];
}

- (void)configureNaviBar {
    
//    WeakSelf;
    
    self.title = @"项目详情";
    
//    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"项目详情", @"预订须知"]];
//    segmentedControl.frame = CGRectMake(0, 0, SCREEN_WIDTH - 20, 30);
//    segmentedControl.tintColor = ColorNormalBGWhite;
//    segmentedControl.selectedSegmentIndex = 0;
//    segmentedControl.segmentedAction = ^(UISegmentedControl *segmentedControl) {
    
//        StrongSelf;
    
//        BOOL needHideLocation = segmentedControl.selectedSegmentIndex;
//        
//        s_self->_locationIcon.hidden  = needHideLocation;
//        s_self->_locationLabel.hidden = needHideLocation;
//        
//        if (!needHideLocation) {
//            
//            [s_self->_webView mas_remakeConstraints:^(MASConstraintMaker *make) {
//                make.left.equalTo(@10);
//                make.right.equalTo(@(-10));
//                make.top.equalTo(_locationLabel.mas_bottom).offset(25);
//                make.bottom.equalTo(_bottomView.mas_top);
//            }];
//            
//        } else {
//            
//            [s_self->_webView mas_remakeConstraints:^(MASConstraintMaker *make) {
//                make.left.equalTo(@10);
//                make.right.equalTo(@(-10));
//                make.top.equalTo(_tagLabel.mas_bottom).offset(25);
//                make.bottom.equalTo(_bottomView.mas_top);
//            }];
//            
//        }
        
//        NSString *htmlString = segmentedControl.selectedSegmentIndex == 0 ? [s_self.project routeIntro] : [s_self.project bookIntro];
    
//        [s_self->_webView loadHTMLString:htmlString baseURL:nil];
//    };
    
//    self.navigationItem.titleView = segmentedControl;
}

- (void)bindWithProject {
    
//    _titleLabel.text = _project.title;
//    
//    for (NSString *tag in _project.tags) {
//        
//        if (tag == [_project.tags firstObject]) {
//            _tagLabel.text = tag;
//        } else {
//            _tagLabel.text = [_tagLabel.text stringByAppendingString:[NSString stringWithFormat:@" / %@", tag]];
//        }
//    }
//    
//    _locationLabel.text = [_locationLabel.text stringByAppendingString:_project.applyPlace];
    
    _bottomLabel.text = _project.title;
    
    [_webView loadHTMLString:_project.routeIntro baseURL:nil];
}

@end
