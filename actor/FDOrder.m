//
//  FDOrder.m
//  actor
//
//  Created by 王澍宇 on 16/4/2.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDOrder.h"

@implementation FDOrder

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"objectID"      : @"_id",
             @"status"        : @"status",
             @"createDate"    : @"create_at",
             @"title"         : @"packge_title",
             @"departureTime" : @"departure_time",
             @"imageURL"      : @"image_url",
             @"projectID"     : @"project_id",
             @"applyFee"      : @"apply_fee",
             @"count"         : @"count"
             };
}

+ (NSValueTransformer *)createDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [FDDateFormatter dateFromString:value];
    }];
}

+ (NSValueTransformer *)departureTimeJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [FDDateFormatter dateFromString:value];
    }];
}

+ (NSString *)stringForStatus:(FDOrderStatus)status {
    
    NSString *string = @"";
    
    switch (status) {
        case FDOrderStatusNeedPayApplyFee:
            string = @"未支付报名费";
            break;
        case FDOrderStatusNeedReview:
            string = @"已支付报名费";
            break;
        case FDOrderStatusNeedPayProjectFee:
            string = @"审核通过";
            break;
        case FDOrderStatusWaitProjectBegin:
            string = @"项目费已支付";
            break;
        case FDOrderStatusProjectProcessing:
            string = @"正在进行";
            break;
        case FDOrderStatusProjectFinished:
            string = @"项目完成";
            break;
        case FDOrderStatusProjectRefunding:
            string = @"正在退款";
            break;
        case FDOrderStatusProjectRefunded:
            string = @"已经退款";
            break;
        case FDOrderStatusProjectRejected:
            string = @"审核未通过";
            break;
        default:
            break;
    }
    
    return string;
}

@end
