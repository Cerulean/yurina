//
//  FDCheckout.m
//  actor
//
//  Created by 王澍宇 on 16/4/6.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDCheckout.h"

@implementation FDCheckout

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"personName"      : @"person_name",
             @"personTel"       : @"person_tel",
             @"personEmail"     : @"person_email",
             @"personAddress"   : @"person_address",
             @"personCount"     : @"person_count",
             @"orderID"         : @"order_id",
             @"period"          : @"period",
             @"title"           : @"title",
             @"departureTime"   : @"departure_time",
             @"fee"             : @"fee",
             @"projectFee"      : @"project_fee",
             @"vacancy"         : @"vacancies",
             @"cancelIntro"     : @"cancel_desc"
             };
}

+ (NSValueTransformer *)departureTimeJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [FDDateFormatter dateFromString:value];
    }];
}

@end
