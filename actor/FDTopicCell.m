//
//  FDTopicCell.m
//  actor
//
//  Created by 王澍宇 on 16/3/18.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDTopicCell.h"

@implementation FDTopicCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleLabel = [UILabel labelWithText:@"" Color:ColorNormalBGWhite FontSize:36 Alignment:NSTextAlignmentCenter Light:NO];
        
        _introLabel = [UILabel labelWithText:@"" Color:ColorNormalBGWhite FontSize:14 Alignment:NSTextAlignmentCenter Light:NO];
        
        _backgroundImageView = [UIImageView new];
        _backgroundImageView.backgroundColor = ColorNormalPlaceholder;
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImageView.clipsToBounds = YES;
        
        ContentViewAddSubView(_backgroundImageView);
        ContentViewAddSubView(_titleLabel);
        ContentViewAddSubView(_introLabel);
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@90);
            make.centerX.equalTo(@0);
        }];
        
        [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleLabel.mas_bottom).offset(10);
            make.centerX.equalTo(@0);
        }];
        
        [_backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(@0);
        }];
        
    }
    return self;
}

- (void)bindWithModel:(FDBaseModel *)model {
    if (![model isKindOfClass:[FDTopic class]]) {
        return;
    }
    
    FDTopic *topic = (FDTopic *)model;
    
    _titleLabel.text = topic.title;
    
    _introLabel.text = topic.intro;
    
    [_backgroundImageView sd_setImageWithURL:[NSURL URLWithString:topic.imageURL]];
}

@end
