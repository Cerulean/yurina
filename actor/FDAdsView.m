//
//  FDAdsView.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDAdsView.h"

#import "FDWebViewController.h"

@implementation FDAdsView {
    UIImageView *_adsView;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    if (self = [super initWithShadowColor:color Info:info]) {
        
        FDAds *ads = self.info;
        
        _adsView = [UIImageView new];
        
        [_adsView setClipsToBounds:YES];
        [_adsView setContentMode:UIViewContentModeScaleAspectFill];
        [_adsView sd_setImageWithURL:[NSURL URLWithString:ads.imageURL]];
        [_adsView.layer setBorderColor:ColorNormalBGWhite.CGColor];
        [_adsView.layer setBorderWidth:2.0];
        [_adsView.layer setCornerRadius:2.0];
        [_adsView setUserInteractionEnabled:ads.linkURL.length ? YES : NO];
        [_adsView setTapAction:^(UIView *view) {
            
            if (ads.linkURL.length) {
                
                FDWebViewController *webController = [FDWebViewController new];
                webController.linkURL = ads.linkURL;
                
                [UIWindow presentViewController:webController Animated:YES Completion:nil];
                
            }
            
        }];
        
        [self.contentView addSubview:_adsView];
        
        [_adsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(@0);
            make.height.equalTo(@(SCREEN_HEIGHT / 2));
            make.bottom.equalTo(@0);
        }];
        
        [self.cancelButton setImage:FDImageWithName(@"Sale_Close") forState:UIControlStateNormal];
        [self.cancelButton setHidden:YES];
        
        [self.cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(0));
            make.top.equalTo(@(0));
        }];
        
        [self.contentView bringSubviewToFront:self.cancelButton];
        
        WeakSelf;
        
        self.shadowView.userInteractionEnabled = YES;
        self.shadowView.tapAction = ^(UIView *view) {
            
            StrongSelf;
            
            [s_self dismiss];
            
        };
        
        self.firstLine.hidden = YES;
        self.titleLabel.hidden = YES;
        
    }
    return self;
}

@end
