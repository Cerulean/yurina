//
//  main.m
//  actor
//
//  Created by 王澍宇 on 16/3/15.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
