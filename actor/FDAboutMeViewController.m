//
//  FDAboutMeViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDAboutMeViewController.h"
#import "FDModifyViewController.h"
#import "FDAccountService.h"

#import "FDLocationViewController.h"

@interface FDAboutMeViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end

@implementation FDAboutMeViewController {
    FDUserAvatarCell *_avatarCell;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"关于我";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _user = [FDAccountService currentUser];
    
    _avatarCell = [FDUserAvatarCell new];
    [_avatarCell.avatar sd_setImageWithURL:[NSURL URLWithString:_user.avatarURL]];
    
    NSMutableArray *cells = [NSMutableArray new];
    
    UITableViewCell *cell = [self generateCellWithTitle:@"昵称"];
    
    [cells addObject:cell];
    
    cell = [self generateCellWithTitle:@"真实姓名"];
    
    [cells addObject:cell];
    
    cell                           = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];;
    cell.textLabel.text            = @"手机号";
    cell.textLabel.font            = [UIFont systemFontOfSize:14 weight:UIFontWeightLight];
    cell.textLabel.textColor       = ColorCellText;
    cell.detailTextLabel.text      = @"13359209698";
    cell.detailTextLabel.font      = [UIFont systemFontOfSize:14 weight:UIFontWeightLight];
    cell.detailTextLabel.textColor = ColorCellText;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
    line.backgroundColor = ColorCellLine;
    
    [cell addSubview:line];
    
    [cells addObject:cell];
    
    cell = [self generateCellWithTitle:@"邮箱"];
    
    [cells addObject:cell];
    
    cell = [self generateCellWithTitle:@"所在地"];
    
    [cells addObject:cell];
    
//    cell = [self generateCellWithTitle:@"兴趣爱好"];
    
//    [cells addObject:cell];
    
    self.cellsArray = [@[@[_avatarCell], [cells copy]] mutableCopy];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNaviBarType:FDNaviBarTypeWhite];
}

- (void)naviToLocationController {
    
    FDLocationViewController *provinceController = [FDLocationViewController new];
    provinceController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"location/provinces" CellClass:[FDLocationCell class]];
    provinceController.title = @"选择省";
    provinceController.callback = ^(id object) {
        
        FDLocation *location = (FDLocation *)object;
        
        [FDAccountService modifyInfoWithParms:@{@"city_id" : location.objectID} Callback:^(BOOL success) {
            
        }];
    };
    
    FDNavigationController *naviController = [[FDNavigationController alloc] initWithRootViewController:provinceController];
    
    [self.navigationController presentViewController:naviController animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        NSMutableArray *blocks = [NSMutableArray new];
        
        FDNormalBlock block = ^ {
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                return ;
            }
            
            UIImagePickerController *pickerController = [UIImagePickerController new];
            pickerController.allowsEditing = YES;
            pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            pickerController.delegate = self;
            
            [self presentViewController:pickerController animated:YES completion:nil];
        };
        
        [blocks addObject:block];
        
        block = ^ {
            
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                return ;
            }
            
            UIImagePickerController *pickerController = [UIImagePickerController new];
            pickerController.allowsEditing = YES;
            pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pickerController.delegate = self;
            
            [self presentViewController:pickerController animated:YES completion:nil];
        };
        
        [blocks addObject:block];
        
        [FDAlert actionSheetWithTitle:@"请选择上传方式" Message:nil Titles:@[@"拍照", @"相册"] Actions:[blocks copy]];
        
    } else if (indexPath.section == 1) {
        FDModifyViewController *modifyViewController = [FDModifyViewController new];
        
        NSString *key    = nil;
        NSString *prompt = nil;
        
        switch (indexPath.row) {
            case 0:
                key    = @"nickname";
                prompt = @"昵称限制20个字符";
                break;
            case 1:
                key    = @"truename";
                prompt = @"请输入真实姓名";
                break;
            case 2:
                break;
            case 3:
                key    = @"email";
                prompt = @"请输入邮箱";
                break;
            case 4:
                
                [self naviToLocationController];
                
                break;
            case 5:
                key    = @"hobbies";
                prompt = @"请输入爱好，用半角空格隔开";
                break;
            default:
                break;
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (key) {
            modifyViewController.key   = key;
            modifyViewController.title = cell.textLabel.text;
            modifyViewController.prompt = prompt;
            
            [self.navigationController pushViewController:modifyViewController animated:YES];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? 70 : 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 9;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [FDAccountService updateAvatarWithImage:image Parms:@{} Callback:^(BOOL success) {
            if (success) {
                [_avatarCell.avatar setImage:image];
            }
        }];
    }];
}

@end
