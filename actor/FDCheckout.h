//
//  FDCheckout.h
//  actor
//
//  Created by 王澍宇 on 16/4/6.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"
#import "FDDateFormatter.h"

@interface FDCheckout : FDBaseModel

@property (nonatomic, strong) NSString *personName;

@property (nonatomic, strong) NSString *personTel;

@property (nonatomic, strong) NSString *personEmail;

@property (nonatomic, strong) NSString *personAddress;

@property (nonatomic, assign) NSInteger personCount;

@property (nonatomic, strong) NSString *period;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *orderID;

@property (nonatomic, strong) NSString *cancelIntro;

@property (nonatomic, strong) NSDate *departureTime;

@property (nonatomic, assign) NSInteger vacancy;

@property (nonatomic, assign) float fee;

@property (nonatomic, assign) float projectFee;

@end
