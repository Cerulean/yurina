//
//  FDTransition.h
//  actor
//
//  Created by 王澍宇 on 16/3/30.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    FDTransitionTypeDefault,
    FDTransitionTypePresent,
    FDTransitionTypeDismiss,
} FDTransitionType;

@interface FDTransition : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) FDTransitionType type;

+ (instancetype)transitionWithType:(FDTransitionType)type;

@end
