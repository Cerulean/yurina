//
//  FDProjectPeopleView.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectPeopleView.h"
#import "FDProjectApplyView.h"
#import "FDProjectService.h"
#import "FDOrderService.h"

#import "FDProjectPackageView.h"

@implementation FDProjectPeopleView {
    
    UILabel *_nameLabel;
    UILabel *_telLabel;
    UILabel *_mailLabel;
    UILabel *_addressLabel;
    UILabel *_countLabel;
    
    UITextField *_nameField;
    UITextField *_telField;
    UITextField *_mailField;
    UITextField *_addressField;
    UITextField *_countField;
    
    UILabel *_tipLabel;
    
    FDButton *_backButton;
    FDButton *_confirmButton;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    
    if (self = [super initWithShadowColor:color Info:info]) {
        
        [self clearPeopleInfo];
        
        self.titleLabel.text = @"填写收货信息";
        
        WeakSelf;
        
        _nameLabel = [UILabel labelWithText:@"姓名 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _telLabel = [UILabel labelWithText:@"电话 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _mailLabel = [UILabel labelWithText:@"邮箱 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _addressLabel = [UILabel labelWithText:@"收货地址 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _countLabel = [UILabel labelWithText:@"购买件数 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _nameField = [UITextField new];
        _nameField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _nameField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入姓名" Color:ColorTripTipText FontSize:16];
        _nameField.beginAction = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self moveHeigher];
        };
        _nameField.doneAction  = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self->_telField becomeFirstResponder];
        };
        
        _telField = [UITextField new];
        _telField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _telField.keyboardType = UIKeyboardTypeNumberPad;
        _telField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入手机号码" Color:ColorTripTipText FontSize:16];
        _telField.doneAction = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self->_mailField becomeFirstResponder];
        };
        
        _mailField = [UITextField new];
        _mailField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _mailField.keyboardType = UIKeyboardTypeEmailAddress;
        _mailField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入常用邮箱" Color:ColorTripTipText FontSize:16];
        _mailField.doneAction = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self->_addressField becomeFirstResponder];
        };
        
        _addressField = [UITextField new];
        _addressField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _addressField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入收货地址" Color:ColorTripTipText FontSize:16];
        _addressField.doneAction = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self->_countField becomeFirstResponder];
        };
        
        _countField = [UITextField new];
        _countField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _countField.keyboardType = UIKeyboardTypeNumberPad;
        _countField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入购买数量" Color:ColorTripTipText FontSize:16];
        _countField.doneAction = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self moveLower];
        };
        
        ContentViewAddSubView(_nameLabel);
        ContentViewAddSubView(_telLabel);
        ContentViewAddSubView(_mailLabel);
        ContentViewAddSubView(_addressLabel);
        ContentViewAddSubView(_countLabel);
        
        ContentViewAddSubView(_nameField);
        ContentViewAddSubView(_telField);
        ContentViewAddSubView(_mailField);
        ContentViewAddSubView(_addressField);
        ContentViewAddSubView(_countField);
        
        UIView *firstline = [UIView new];
        firstline.backgroundColor = ColorTripTipText;
        
        UIView *secondLine = [UIView new];
        secondLine.backgroundColor = ColorTripTipText;
        
        UIView *thirdLine = [UIView new];
        thirdLine.backgroundColor = ColorTripTipText;
        
        UIView *fourthLine = [UIView new];
        fourthLine.backgroundColor = ColorTripTipText;
        
        UIView *fifthLine = [UIView new];
        fifthLine.backgroundColor = ColorTripTipText;
        
        ContentViewAddSubView(firstline);
        ContentViewAddSubView(secondLine);
        ContentViewAddSubView(thirdLine);
        ContentViewAddSubView(fourthLine);
        ContentViewAddSubView(fifthLine);
        
//        _tipLabel = [UILabel labelWithText:@"添加出行人员+"
//                                     Color:ColorProjectlTipText
//                                  FontSize:10
//                                 Alignment:NSTextAlignmentCenter
//                                     Light:NO];
        
//        _tipLabel.userInteractionEnabled = YES;
//        _tipLabel.hidden = YES;
//        _tipLabel.tapAction = ^(UIView *view) {
//            
//            StrongSelf;
//            
//            if ([s_self validateInfoIfLegal]) {
//                
//                [s_self addPerson];
//                
//                [s_self->_nameField setText:@""];
//                [s_self->_telField  setText:@""];
//                [s_self->_mailField setText:@""];
//            }
//            
//        };
        
        _backButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"上一步" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            NSMutableDictionary *parms = [FDOrderService applyParms];
            parms[@"people"] = nil;
            
            FDProjectApplyView *applyView = [[FDProjectApplyView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9] Info:s_self.info];
            
            [s_self dismiss];
            
            [applyView showWithView:s_self.superview];
            
        }];
        
        [_backButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_backButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        
        _confirmButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"下一步" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            NSMutableDictionary *parms = [FDOrderService applyParms];
            
            NSMutableArray *people = parms[@"people"];
            
            if (!people.count) {
                
                if ([s_self validateInfoIfLegal]) {
                    
                    for (NSInteger index = 0; index < [s_self->_countField.text integerValue]; ++index) {
                        [s_self addPerson];
                    }
                    
                } else {
                    return;
                }
                
            }
            
            FDProjectPackageView *packageView =  [[FDProjectPackageView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9]
                                                                                              Info:s_self.info];
            
            [s_self dismiss];
            
            [packageView showWithView:s_self.superview];
            
        }];
        
        [_confirmButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_confirmButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        
        ContentViewAddSubView(_tipLabel);
        ContentViewAddSubView(_confirmButton);
        ContentViewAddSubView(_backButton);
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(self.firstLine.mas_bottom).offset(30);
        }];
        
        [_nameField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_nameLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [firstline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_nameField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(firstline.mas_bottom).offset(30);
        }];
        
        [_telField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_telLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_telField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_mailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(secondLine.mas_bottom).offset(30);
        }];
        
        [_mailField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_mailLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [thirdLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_mailField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(thirdLine.mas_bottom).offset(30);
        }];
        
        [_addressField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_addressLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [fourthLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_addressField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(fourthLine.mas_bottom).offset(30);
        }];
        
        [_countField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_countLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [fifthLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_countField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
//
//        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(fourthLine.mas_bottom).offset(10);
//            make.left.equalTo(_nameField);
//        }];
        
        [_backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(fifthLine).offset(20);
            make.right.equalTo(self.mas_centerX).offset(-10);
            make.height.equalTo(@45);
        }];
        
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_centerX).offset(10);
            make.right.equalTo(@(-20));
            make.top.equalTo(_backButton);
            make.height.equalTo(@45);
            make.bottom.equalTo(@(-20));
        }];
        
    }
    return self;
}

#pragma mark - Helper Method

- (void)clearPeopleInfo {
    NSMutableDictionary *parms = [FDOrderService applyParms];
    parms[@"people"] = nil;
}

- (void)addPerson {
    
    FDPerson *person = [FDPerson new];
    person.name    = _nameField.text;
    person.tel     = _telField.text;
    person.email   = _mailField.text;
    person.address = _addressField.text;
    
    NSMutableDictionary *parms = [FDOrderService applyParms];
    NSMutableArray *people = parms[@"people"];
    
    if (!people) {
        people = [NSMutableArray new];
    }
    
    [people addObject:[MTLJSONAdapter JSONDictionaryFromModel:person error:nil]];
    
    parms[@"people"] = people;
}

- (BOOL)validateInfoIfLegal {
    
    if (!_nameField.text.length) {
        [FDAlert alertWithTitle:@"错误" Message:@"姓名不能为空"];
        return NO;
    }
    
    if (![FDValidater validateMobile:_telField.text]) {
        [FDAlert alertWithTitle:@"错误" Message:@"手机格式不合法"];
        return NO;
    }
    
    if (![FDValidater validateEmail:_mailField.text]) {
        [FDAlert alertWithTitle:@"错误" Message:@"邮箱格式非法"];
        return NO;
    }
    
    if (!_addressField.text.length) {
        [FDAlert alertWithTitle:@"错误" Message:@"收货地址不能为空"];
        return NO;
    }
    
    if (!_countField.text.length || [_countField.text integerValue] < 1) {
        [FDAlert alertWithTitle:@"错误" Message:@"购买数量不合法"];
        return NO;
    }
    
    return YES;
}


@end
