//
//  FDLocation.m
//  actor
//
//  Created by 王澍宇 on 16/4/1.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDLocation.h"

@implementation FDLocation

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"objectID" : @"_id",
             @"name"     : @"name",
             };
}

@end
