//
//  FDApplyView.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectApplyView.h"

#import "FDProjectPeopleView.h"
#import "FDProjectService.h"
#import "FDOrderService.h"

@implementation FDProjectApplyView {
    
    UIImageView *_imageView;
    
    UILabel *_nameLabel;
    
    UILabel *_tagLabel;
    
    UILabel *_introLabel;
    
    UIView *_secondLine;
    
    UILabel *_tipLabel;
    
    FDButton *_nextButton;
    
    NSMutableArray *_buttons;
    
    BOOL _canGoNext;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    if (self = [super initWithShadowColor:color Info:info]) {
        
        self.titleLabel.text = @"立刻购买";
        
        FDProject *project = (FDProject *)info;
        
        _imageView = [UIImageView new];
        
        [_imageView setBackgroundColor:ColorNormalPlaceholder];
        [_imageView setContentMode:UIViewContentModeScaleAspectFill];
        [_imageView setClipsToBounds:YES];
        [_imageView sd_setImageWithURL:[NSURL URLWithString:[project.imageURLs firstObject]]];
        
        _nameLabel = [UILabel labelWithText:project.title Color:ColorCellText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
        _nameLabel.numberOfLines = 2;

        _tagLabel  = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        for (NSString *tag in project.tags) {
            
            if (tag == [project.tags firstObject]) {
                _tagLabel.text = tag;
            } else {
                _tagLabel.text = [_tagLabel.text stringByAppendingString:[NSString stringWithFormat:@" / %@", tag]];
            }
        }
        
        FDPackage *package = [[project packages] firstObject];
        
        NSMutableAttributedString *introString = [[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@"CNY %.2f · ", package.fee / 100]
                                                                                           Color:ColorProjectMoreButton
                                                                                        FontSize:10] mutableCopy];
        
        [introString appendAttributedString:[NSAttributedString attributedStringWithString:[NSString stringWithFormat:@"%ld天 · %@",
                                                                                            package.duration,
                                                                                            project.applyPlace]
                                                                                     Color:ColorProjectPlaceText FontSize:10]];
        
        _introLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        _introLabel.attributedText = introString;
        
        _secondLine = [UIView new];
        _secondLine.backgroundColor = ColorCellLine;
        
        _tipLabel = [UILabel labelWithText:@"选择义卖期数" Color:ColorCellText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        WeakSelf;
        
        _nextButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"下一步" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            if (!s_self->_canGoNext) {
                return [FDAlert alertWithTitle:@"错误" Message:@"未选择期数"];
            }
            
            FDProjectPeopleView *peopleView = [[FDProjectPeopleView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9]
                                                                                          Info:project];
            
            [s_self dismiss];
            
            [peopleView showWithView:s_self.superview];
        }];
        
        [_nextButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_nextButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:YES AutoSelected:NO];
        
        ContentViewAddSubView(_imageView);
        ContentViewAddSubView(_nameLabel);
        ContentViewAddSubView(_tagLabel);
        ContentViewAddSubView(_introLabel);
        ContentViewAddSubView(_secondLine);
        ContentViewAddSubView(_tipLabel);
        ContentViewAddSubView(_nextButton);
        
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.firstLine.mas_bottom).offset(30);
            make.left.equalTo(@20);
            make.size.mas_equalTo(CGSizeMake(90, 60));
        }];
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_imageView);
            make.left.equalTo(_imageView.mas_right).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [_tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_nameLabel.mas_bottom).offset(10);
        }];
        
        [_introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_imageView);
            make.top.equalTo(_imageView.mas_bottom).offset(10);
        }];
        
        [_secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.top.equalTo(_introLabel.mas_bottom).offset(10);
            make.height.equalTo(@0.5);
        }];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(_secondLine.mas_bottom).offset(20);
        }];
        
        UIScrollView *scrollView = [UIScrollView new];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator   = NO;
        
        _buttons = [NSMutableArray new];
        
        for (NSInteger index = 0; index < [project.departureTimes count]; ++index) {
            
            FDDepartureTime *time = project.departureTimes[index];
          
            NSDateComponents *components = [FDDateFormatter dateComponentsFromDate:time.startTime];
           
            NSString *infoString = [NSString stringWithFormat:@"%@ %ld年%ld月%ld日 剩余名额 %ld 名", time.period, components.year, components.month, components.day, time.vacancy];

            FDButton *timeButton = [FDButton buttonWithType:UIButtonTypeCustom Title:infoString FontSize:10 ActionBlock:^(FDButton *button) {
                
                StrongSelf;
                
                [s_self updateButtonState:button];
                
                if (button.selected) {
                    
                    NSMutableDictionary *parms = [FDOrderService applyParms];
                    parms[@"project_id"]        = project.objectID;
                    parms[@"departure_time_id"] = @(time.objectID.integerValue);
                    
                    s_self->_canGoNext = YES;
                    
                } else {
                    
                    s_self->_canGoNext = NO;
                    
                }
            }];
        
            [timeButton.layer setBorderColor:ColorCellText.CGColor];
            [timeButton.layer setBorderWidth:0.5];
            [timeButton.layer setCornerRadius:2.0];
            [timeButton setFrame:CGRectMake(20, (30 + 10) * index, SCREEN_WIDTH - 40 * 2, 30)];
            [timeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [timeButton setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
            [timeButton setImageName:@"Project_Checkout_Form" AutoHighlight:NO AutoDisabled:NO AutoSelected:YES];
            [timeButton setImageEdgeInsets:UIEdgeInsetsMake(0, SCREEN_WIDTH - 40 * 2 - 30, 0, 0)];
            [timeButton setEnabled:time.vacancy > 0 ? YES : NO];
            
            [scrollView addSubview:timeButton];
            
            [_buttons addObject:timeButton];
        }
        
        scrollView.contentSize = CGSizeMake(SCREEN_WIDTH - 35 * 2, 40 * [project.departureTimes count] + 30);
        
        ContentViewAddSubView(scrollView);
        
        CGFloat contentHeight = scrollView.contentSize.height;
        
        [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(0));
            make.right.equalTo(@(0));
            make.height.equalTo(contentHeight > SCREEN_HEIGHT / 5 ? @(SCREEN_HEIGHT / 5) : @(contentHeight));
            make.top.equalTo(_tipLabel.mas_bottom).offset(10);
        }];
        
        [_nextButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(scrollView.mas_bottom).offset(20);
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.height.equalTo(@45);
            make.bottom.equalTo(@(-20));
        }];
        
    }
    return self;
}

#pragma mark - Helper Method

- (void)updateButtonState:(FDButton *)abutton {
    
    for (FDButton *button in _buttons) {
        
        if (abutton == button) {
            continue;
        }
        
        button.selected = NO;
        
        [button.layer setBorderColor:ColorCellText.CGColor];
    }
    
    [abutton setSelected:!abutton.selected];
    
    [abutton.layer setBorderColor:abutton.selected ? ColorProjectApplyLabelBG.CGColor : ColorCellText.CGColor];
}

@end
