//
//  AppDelegate.m
//  actor
//
//  Created by 王澍宇 on 16/3/15.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "AppDelegate.h"

#import "FDAccountService.h"

#import "FDTabBarController.h"
#import "FDFavoViewController.h"
#import "FDUserViewController.h"
#import "FDBaseViewController.h"
#import "FDOrderViewController.h"
#import "FDTopicViewController.h"
#import "FDWelcomeViewController.h"
#import "FDExplorerViewController.h"

@interface AppDelegate () <WXApiDelegate, WeiboSDKDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [AVOSCloud registerForRemoteNotification];
    
    [AVOSCloud setApplicationId:@"iVwR67XoMu7fCl68HJxB1SuG-gzGzoHsz"
                      clientKey:@"HbThoT8jV8RnSiVUCWtHnYJV"];
    
    [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
#ifdef DEBUG
    [AVPush setProductionMode:NO];
#else
    [AVPush setProductionMode:YES];
#endif
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    FDTopicViewController *topicViewController = [[FDTopicViewController alloc] init];
    topicViewController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"project/topics" CellClass:[FDTopicCell class]];
    
    FDNavigationController *topicNavigationController = [[FDNavigationController alloc] initWithRootViewController:topicViewController];
    
    FDExplorerViewController *explorerViewController = [[FDExplorerViewController alloc] init];
    explorerViewController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"project/explore" CellClass:[FDProjectCell class]];
    
    FDNavigationController *explorerNavigationController = [[FDNavigationController alloc] initWithRootViewController:explorerViewController];
    
//    FDFavoViewController *favoViewController = [[FDFavoViewController alloc] init];
//    favoViewController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"project/favo" CellClass:[FDProjectCell class]];
//    favoViewController.enableDownRefresh = NO;
    
//    FDNavigationController *favoNavigationController = [[FDNavigationController alloc] initWithRootViewController:favoViewController];
    
    FDOrderViewController *orderViewController = [[FDOrderViewController alloc] init];
    orderViewController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"order/list" CellClass:[FDOrderCell class]];
    orderViewController.viewModel.prams[@"order_status"] = @"1";
    
    FDNavigationController *orderNavigationController = [[FDNavigationController alloc] initWithRootViewController:orderViewController];
    
    FDUserViewController *userViewController = [[FDUserViewController alloc] init];
    
    FDNavigationController *userNavigationController = [[FDNavigationController alloc] initWithRootViewController:userViewController];
    
    FDTabBarController *tabBarController = [[FDTabBarController alloc] initWithViewControllers:@[explorerNavigationController,
                                                                                                 topicNavigationController,
//                                                                                                 favoNavigationController,
                                                                                                 orderNavigationController,
                                                                                                 userNavigationController]
                                                                                        Titles:@[@"义卖", @"专题", @"订单", @"我的"]
                                                                                        Images:@[FDImageWithName(@"Tab_Main"),
                                                                                                 FDImageWithName(@"Tab_Explorer"),
//                                                                                                 FDImageWithName(@"Tab_Favo"),
                                                                                                 FDImageWithName(@"Tab_Trip"),
                                                                                                 FDImageWithName(@"Tab_Mine")]
                                                                                SelectedImages:@[FDImageWithName(@"Tab_Main_Selected"),
                                                                                                 FDImageWithName(@"Tab_Explorer_Selected"),
//                                                                                                 FDImageWithName(@"Tab_Favo_Selected"),
                                                                                                 FDImageWithName(@"Tab_Trip_Selected"),
                                                                                                 FDImageWithName(@"Tab_Mine_Selected")]];
    tabBarController.tabBar.alpha = 0.95;
    
    [self.window setRootViewController:tabBarController];
    [self.window makeKeyAndVisible];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:ColorNormalBGWhite];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorNormalNaviTitle}];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if ([FDAccountService checkIfNeedLogin]) {
            return ;
        }
        
        [FDAccountService getInfoWithParms:@{} Callback:nil];
    });
    
    return YES;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    AVInstallation *currentInstallation = [AVInstallation currentInstallation];
    
    [currentInstallation setDeviceTokenFromData:deviceToken];
    
    [currentInstallation saveInBackground];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([Pingpp handleOpenURL:url withCompletion:nil]) {
        return YES;
    }
    
    if ([WeiboSDK handleOpenURL:url delegate:self]) {
        return YES;
    }
    
    if ([WXApi handleOpenURL:url delegate:self]) {
        return YES;
    }
    
    return NO;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    
    if ([Pingpp handleOpenURL:url withCompletion:nil]) {
        return YES;
    }
    
    if ([WeiboSDK handleOpenURL:url delegate:self]) {
        return YES;
    }
    
    if ([WXApi handleOpenURL:url delegate:self]) {
        return YES;
    }
    
    return NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Weibo Callback

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response {
    
    if ([response isKindOfClass:WBAuthorizeResponse.class]) {
        
        WBAuthorizeResponse *authorizeResp = (WBAuthorizeResponse *)response;
        
        if (!authorizeResp.accessToken.length) {
            return [SVProgressHUD showErrorWithStatus:@"授权失败"];
        }
        
        [FDAccountService loginWithPlatform:FDLoginPlatformTypeWeibo Parms:@{@"weibo_id"     : authorizeResp.userID,
                                                                             @"access_token" : authorizeResp.accessToken}];
        
    }
}

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request {
    
}

#pragma mark - Wechat Callback

- (void)onResp:(BaseResp *)resp {
    
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        
        if (resp.errCode != 0) {
            return [SVProgressHUD showErrorWithStatus:resp.errStr];
        }
        
        [FDAccountService loginWithPlatform:FDLoginPlatformTypeWechat Parms:@{@"code" : [(SendAuthResp *)resp code]}];
        
    }
}

@end
