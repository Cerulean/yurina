//
//  FDUserViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDUserViewController.h"
#import "FDAccountService.h"

#import "FDAboutMeViewController.h"
#import "FDSettingViewController.h"
#import "FDAbousUsViewController.h"

#import "FDProjectConsultView.h"

@interface FDUserViewController ()

@end

@implementation FDUserViewController {
    FDUserCell *_userCell;
    FDUser *_user;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"个人中心";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _userCell = [FDUserCell new];
    
    NSMutableArray *cells = [NSMutableArray new];
    
    FDSettingCell *cell  = [FDSettingCell new];
    cell.icon.image      = FDImageWithName(@"Account_Contact");
    cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
    cell.titleLabel.text = @"联系我们";
    
    [cells addObject:cell];
    
//    cell  = [FDSettingCell new];
//    cell.icon.image      = FDImageWithName(@"Account_About");
//    cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
//    cell.titleLabel.text = @"关于我们";
    
//    [cells addObject:cell];
    
    cell  = [FDSettingCell new];
    cell.icon.image      = FDImageWithName(@"Account_Setting");
    cell.accessoryType   = UITableViewCellAccessoryDisclosureIndicator;
    cell.titleLabel.text = @"设置";

    self.cellsArray = [@[@[_userCell], [cells copy], @[cell]] mutableCopy];
    
    [self loadNotificationCallback];
}

- (void)loadNotificationCallback {
    
    WeakSelf;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kLogoutNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      
                                                      StrongSelf;
                                                      
                                                      s_self->_userCell.nameLabel.text = @"超级活动家";
                                                      s_self->_userCell.avatar.image = nil;
                                                      
                                                  }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([FDAccountService checkIfNeedLogin]) {
        return [FDAccountService promptLogin];
    }
    
    [FDAccountService getInfoWithParms:@{} Callback:^(FDUser *user) {
        if (user) {
            
            [self setUser:user];
            [_userCell.nameLabel setText:_user.nickname];
            [_userCell.avatar sd_setImageWithURL:[NSURL URLWithString:_user.avatarURL] placeholderImage:nil options:SDWebImageRefreshCached];
            
        }
    }];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0 || indexPath.section == 2) {
        if ([FDAccountService checkIfNeedLogin]) {
            return [FDAccountService promptLogin];
        }
        
        if (indexPath.section == 0) {
            
            FDAboutMeViewController *aboutMeController = [FDAboutMeViewController new];
            
            [self.navigationController pushViewController:aboutMeController animated:YES];
        } else {
            
            FDSettingViewController *settingController = [FDSettingViewController new];
            
            [self.navigationController pushViewController:settingController animated:YES];
        }
    } else {
        
        if (indexPath.row == 0) {
            
            FDProjectConsultView *consultView = [[FDProjectConsultView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9] Info:nil];
            
            [consultView showWithView:self.navigationController.view];
            
        } else {
            
            FDAbousUsViewController *aboutUsController = [FDAbousUsViewController new];
            
            [self.navigationController pushViewController:aboutUsController animated:YES];
            
        }
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? 100 : 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 9;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

@end
