//
//  FDCredential.m
//  actor
//
//  Created by 王澍宇 on 16/4/5.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDCredential.h"

@implementation FDCredential

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"personName"      : @"person_name",
             @"personTel"       : @"person_tel",
             @"personEmail"     : @"person_email",
             @"personAddress"   : @"person_address",
             @"count"           : @"count",
             @"packageName"     : @"package_name",
             @"period"          : @"period",
             @"title"           : @"title",
             @"channel"         : @"channel",
             @"orderID"         : @"order_id",
             @"orderCreateDate" : @"order_create_date",
             @"orderStatus"     : @"order_status",
             @"applyFee"        : @"apply_fee",
             @"projectFee"      : @"project_fee",
             };
}

+ (NSValueTransformer *)orderCreateDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [FDDateFormatter dateFromString:value];
    }];
}

+ (NSValueTransformer *)channelJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if (![value isKindOfClass:[NSString class]]) {
            return nil;
        }
        
        if ([value isEqualToString:@"wx"]) {
            return @"微信支付";
        } else if ([value isEqualToString:@"alipay"]) {
            return @"支付宝支付";
        } else {
            return @"其他渠道";
        }
        
    }];
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"";
        self.personName  = @"";
        self.personTel   = @"";
        self.personEmail = @"";
        self.period  = @"";
        self.channel = @"";
    }
    return self;
}

@end
