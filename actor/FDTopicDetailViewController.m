//
//  FDTopicDetailViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/22.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDTopicDetailViewController.h"

@implementation FDTopicDetailViewController {
    UIView *_headerView;
    
    UIImageView *_headerImageView;
    
    UIImageView *_headerTagView;
    
    UILabel *_headerTagLabel;
    
    UILabel *_headerTitleLabel;
    
    UIView *_headerLine;
    
    UILabel *_headerIntroLabel;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadHeaderView];
    
    WeakSelf;
    
    [self.viewModel fetchModelWithType:FDFetchModeNew Callback:^(NSArray *objects, FDBaseModel *model, NSError *error) {
        
        StrongSelf;
        
        FDTopic *topic = (FDTopic *)model;
        
        [s_self.viewModel setObjects:[[topic projects] mutableCopy]];
        [s_self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        [s_self updateHeaderViewWithTopic:topic];
        
    }];
}

- (void)loadHeaderView {
    
    _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 605)];
    
    _headerImageView = [UIImageView new];
    _headerImageView.backgroundColor = ColorNormalPlaceholder;
    _headerImageView.contentMode = UIViewContentModeScaleAspectFill;
    _headerImageView.clipsToBounds = YES;
    
    _headerTagView = [UIImageView new];
    _headerTagView.backgroundColor = ColorNormalPlaceholder;
    _headerTagView.clipsToBounds = YES;
    _headerTagView.contentMode = UIViewContentModeScaleAspectFit;
    _headerTagView.layer.cornerRadius = 25;
    _headerTagView.backgroundColor = ColorNormalBGWhite;
    
    _headerTagLabel = [UILabel labelWithText:@"" Color:ColorTopicText FontSize:12 Alignment:NSTextAlignmentCenter Light:YES];
    
    _headerTitleLabel = [UILabel labelWithText:@"" Color:ColorTopicText FontSize:14 Alignment:NSTextAlignmentLeft Light:NO];
    
    _headerLine = [UIView new];
    _headerLine.backgroundColor = ColorTopicTipText;
    
    _headerIntroLabel = [UILabel labelWithText:@"" Color:ColorTopicText FontSize:12 Alignment:NSTextAlignmentLeft Light:YES];
    
    [_headerView addSubview:_headerImageView];
    [_headerView addSubview:_headerTagView];
    [_headerView addSubview:_headerTagLabel];
    [_headerView addSubview:_headerTitleLabel];
    [_headerView addSubview:_headerLine];
    [_headerView addSubview:_headerIntroLabel];
    
    [_headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(@0);
        make.height.equalTo(@465);
    }];
    
    [_headerTagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@30);
        make.centerY.equalTo(_headerImageView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [_headerTagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headerTagView.mas_bottom).offset(5);
        make.centerX.equalTo(_headerTagView);
    }];
    
    [_headerTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_headerTagLabel);
        make.top.equalTo(_headerTagLabel.mas_bottom).offset(20);
    }];
    
    [_headerLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headerTitleLabel.mas_bottom).offset(5);
        make.left.equalTo(_headerTitleLabel);
        make.right.equalTo(@(-150));
        make.height.equalTo(@0.5);
    }];
    
    [_headerIntroLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_headerTitleLabel);
        make.top.equalTo(_headerLine.mas_bottom).offset(10);
    }];
    
    self.tableView.tableHeaderView = _headerView;
}

- (void)updateHeaderViewWithTopic:(FDTopic *)topic {
    [_headerImageView sd_setImageWithURL:URLWithString(topic.imageURL)];
    [_headerTagView sd_setImageWithURL:URLWithString(topic.tag.imageURL)];
    [_headerTagLabel setText:topic.tag.name];
    [_headerTitleLabel setText:topic.title];
    [_headerIntroLabel setText:topic.intro];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300;
}

@end
