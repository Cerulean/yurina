//
//  FDLocationViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/1.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDLocationViewController.h"

@implementation FDLocationViewController

- (instancetype)init {
    if (self = [super init]) {
        self.enableDownRefresh = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    FDBaseModel *location = self.viewModel.objects[indexPath.row];
    
    if ([self.viewModel.api containsString:@"province"]) {
        
        WeakSelf;
        
        FDLocationViewController *cityController = [FDLocationViewController new];
        cityController.title = @"选择城市";
        cityController.viewModel = [[FDBaseViewModel alloc] initWithAPI:@"location/cities" CellClass:[FDLocationCell class]];
        cityController.viewModel.prams[@"province_id"] = location.objectID;
        cityController.callback = ^(id object) {
            
            StrongSelf;
            
            if (s_self.callback) {
                s_self.callback(object);
            }
        };
        
        [self.navigationController pushViewController:cityController animated:YES];
        
    } else {
        
        if (self.callback) {
            self.callback(location);
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

@end
