//
//  FDDefaultInfoView.h
//  actor
//
//  Created by 王澍宇 on 16/4/6.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDInfoView.h"
#import "FDAccountService.h"
#import "FDTabBarController.h"

@interface FDDefaultInfoView : FDInfoView

- (instancetype)initWithHeadText:(NSString *)headText TipText:(NSString *)tipText Image:(UIImage *)image BackgounrdImage:(UIImage *)backgroundImage;

@end
