//
//  KeyValueSheet.h
//  maruko
//
//  Created by 王澍宇 on 16/2/22.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#ifndef KeyValueSheet_h
#define KeyValueSheet_h

#define kTokenKey    @"token"
#define kResultsKey  @"results"

#define kObjectIDKey @"_id"

#define kChannelWechat @"wx"
#define kChannelAlipay @"alipay"

#define kURLSchemeWechat @"wx81bc33b2886ddff2"
#define kURLSchemeAlipay @"ap2016021601145906"

#define kAppIDWechat @"wx81bc33b2886ddff2"
#define kAppIDWeibo  @"3659883749"

#define kSSOTypeKey @"SSO_TYPE"
#define kSSOIDKey   @"SSO_ID"

#ifdef DEBUG
#define kRedirectURLWeibo @"http://api.in.actopper.com/v1/account/sso"
#define kURLDuty @"http://www.qq.com"
#else
#define kRedirectURLWeibo @"http://api.actopper.com/v1/account/sso"
#define kURLDuty @"http://www.qq.com"
#endif

#endif /* KeyValueSheet_h */
