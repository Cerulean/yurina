//
//  FDConsultView.m
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectConsultView.h"
#import "FDProjectConsultDetailView.h"

@implementation FDProjectConsultView

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    if (self = [super initWithShadowColor:color Info:info]) {
        self.titleLabel.text = @"咨询客服";
        
        WeakSelf;
        
        FDButton *_consultButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"提交联系信息" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            [s_self dismiss];
            
            FDProjectConsultDetailView *detailView = [[FDProjectConsultDetailView alloc] initWithShadowColor:[UIColor colorWithHex:0x4A4A4A andAlpha:0.9] Info:nil];
            
            [detailView showWithView:s_self.superview];
            
        }];
        
        [_consultButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        [_consultButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        
        FDButton *_telButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"拨打客服电话" FontSize:14 ActionBlock:^(FDButton *button) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://18629357335"]];
        }];
        
        [_telButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        [_telButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        
        [self.contentView addSubview:_consultButton];
        [self.contentView addSubview:_telButton];
        
        [_consultButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.top.equalTo(self.firstLine.mas_bottom).offset(30);
            make.height.equalTo(@45);
        }];
        
        [_telButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.height.equalTo(_consultButton);
            make.top.equalTo(_consultButton.mas_bottom).offset(30);
            make.bottom.equalTo(@(-30));
        }];
    }
    return self;
}

@end
