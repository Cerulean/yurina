//
//  FDAccountService.m
//  maruko
//
//  Created by 王澍宇 on 16/2/22.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDAccountService.h"

#import "FDWelcomeViewController.h"

static FDUser *_currentUser;

@implementation FDAccountService

+ (void)load {
    [[NSNotificationCenter defaultCenter] addObserverForName:kNeedLoginNotification
                                                      object:[FDNetworkEngine sharedEngine]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      
                                                      [FDAccountService promptLogin];
    }];
    
    [WXApi registerApp:kAppIDWechat];
    
    [WeiboSDK registerApp:kAppIDWeibo];
}

+ (FDUser *)currentUser {
    return _currentUser;
}

+ (BOOL)checkIfNeedLogin {
    NSString *token = [[FDNetworkEngine sharedEngine] token];
    return token.length > 0 ? NO : YES;
}


+ (void)loginWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback {
    [FDWebService requestWithAPI:@"account/login" Method:@"POST" Parms:parms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (success) {
            
            [[FDNetworkEngine sharedEngine] setToken:resultDic[kTokenKey]];
            
        }
        
        if (callback) {
            callback(success);
        }
    }];
}

+ (BOOL)canLoginWithWechat {
    return [WXApi isWXAppInstalled];
}

+ (void)purposeLoginWithPlatform:(FDLoginPlatformType)type {
    
    if (type == FDLoginPlatformTypeWechat) {
        
        if (![WXApi isWXAppInstalled]) {
            
            return [SVProgressHUD showErrorWithStatus:@"请安装微信客户端"];
            
        }
        
        SendAuthReq *req = [SendAuthReq new];
        req.scope = @"snsapi_userinfo" ;
        
        [WXApi sendReq:req];
        
    } else if (type == FDLoginPlatformTypeWeibo) {
        
        WBAuthorizeRequest *req = [WBAuthorizeRequest new];
        req.scope = @"all";
        req.redirectURI = kRedirectURLWeibo;
        
        [WeiboSDK sendRequest:req];
        
    } else {
        
        return;
        
    }
}

+ (void)loginWithPlatform:(FDLoginPlatformType)type Parms:(NSDictionary *)parms {
    
    NSMutableDictionary *_parms = [parms mutableCopy];
    _parms[@"sso_type"] = @(type);
    
    [FDWebService requestWithAPI:@"account/sso" Method:@"POST" Parms:_parms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (success) {
            
            if (resultDic[kTokenKey]) {
                
                [[FDNetworkEngine sharedEngine] setToken:resultDic[kTokenKey]];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:KSSOLoginSuccessNotification object:nil];
                
            } else {
                
                NSString *ssoKey = type == FDLoginPlatformTypeWechat ? @"wechat_id" : @"weibo_id";
                
                NSString *ssoID = resultDic[ssoKey];
                
                if (ssoID) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:KSSOLoginNeedBindNotification
                                                                        object:nil
                                                                      userInfo:@{kSSOTypeKey : @(type),
                                                                                 kSSOIDKey   : ssoID }];
                    
                }
                
            }
            
        }
        
    }];
    
}

+ (void)bindSSOWithUsername:(NSString *)username
                   Password:(NSString *)password
                      SsoID:(NSString *)ssoID
                    SsoType:(FDLoginPlatformType)type
                   Callback:(FDWebServiceCallback)callback{
    
    [FDWebService requestWithAPI:@"account/bind"
                          Method:@"POST"
                           Parms:@{@"username" : username,
                                   @"password" : [FDCoding md5:password],
                                   @"sso_id"   : ssoID,
                                   @"sso_type" : @(type)}
                             HUD:YES
                           Block:^(BOOL success, NSDictionary *resultDic) {
                               
                               if (callback) {
                                   
                                   if (success && resultDic[kTokenKey]) {
                                       
                                       [[FDNetworkEngine sharedEngine] setToken:resultDic[kTokenKey]];
                                       
                                       callback(success);
                                   }
                                   
                               }
                           }];
    
}

+ (void)logoutWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback {
    
    [FDWebService requestWithAPI:@"account/logout" Method:@"POST" Parms:@{} HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (callback) {
            
            if (success) {
                
                [[FDNetworkEngine sharedEngine] setToken:nil];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kLogoutNotification object:self];
                
            }
            
            callback(YES);
        }
        
    }];
}

+ (void)registerWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback {
    [FDWebService requestWithAPI:@"account/register" Method:@"POST" Parms:parms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (success) {
            [[FDNetworkEngine sharedEngine] setToken:resultDic[kTokenKey]];
        }
        
        if (callback) {
            callback(success);
        }
    }];
}

+ (void)getSMSCodeWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback {
    [FDWebService requestWithAPI:@"account/smscode" Method:@"GET" Parms:parms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            callback(success);
        }
    }];
}

+ (void)forgetPasswordWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback {
    [FDWebService requestWithAPI:@"account/forget" Method:@"POST" Parms:parms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            callback(success);
        }
    }];
}

+ (void)getInfoWithParms:(NSDictionary *)parms Callback:(void (^)(FDUser *))callback {
    [FDWebService requestWithAPI:@"account/info" Method:@"GET" Parms:parms HUD:NO Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (success) {
            _currentUser = [MTLJSONAdapter modelOfClass:[FDUser class] fromJSONDictionary:resultDic error:nil];
        }
        
        if (callback) {
            if (success) {
                callback(_currentUser);
            }
        }
    }];
}

+ (void)modifyInfoWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback {
    [FDWebService requestWithAPI:@"account/info" Method:@"POST" Parms:parms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            callback(success);
        }
    }];
}

+ (void)changePwdWithOldPwd:(NSString *)oldPwd NewPwd:(NSString *)newPwd Callback:(void (^)(BOOL success))callback {
    
    NSDictionary *parms = @{
                            @"old" : [FDCoding md5:oldPwd],
                            @"new" : newPwd,
                            };
    
    [FDWebService requestWithAPI:@"account/password" Method:@"POST" Parms:parms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            callback(success);
        }
    }];
}

+ (void)updateAvatarWithImage:(UIImage *)image Parms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback {
    
    FDFile *file = [FDFile new];
    
    file.name = @"avatar";
    file.data = UIImageJPEGRepresentation(image, 0.3);
    file.fileName = [FDCoding md5:@"avatar"];
    file.mimeType = @"image/jpeg";
    
    [FDWebService requestWithAPI:@"account/avatar" Parms:parms Files:@[file] HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            callback(success);
        }
    } ProgressCallback:nil];
}

+ (void)promptLogin {
    
    
    UIViewController *presentedController = [[UIApplication sharedApplication].keyWindow.rootViewController presentedViewController];
    
    if ([presentedController isKindOfClass:[UIAlertController class]]) {
        return;
    }
    
    [FDAlert alertWithTitle:@"需要登录" Message:@"点击确认登录" Stressed:YES confirmAction:^{
        [FDAccountService showLoginController];
    }];
}

+ (void)showLoginController {
    [UIWindow presentViewController:[FDWelcomeViewController new] Animated:YES Completion:nil];
}


@end
